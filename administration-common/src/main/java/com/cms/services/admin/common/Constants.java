package com.cms.services.admin.common;

/**
 * May 10, 2020
 * 
 * Babu Gali
 */
public class Constants {

	public static final String EQUALS = "equals";
	public static final String GREATERTHANEQUALS = "GreaterThanEquals";
	public static final String LESSTHANEQUALS = "GreaterThanEquals";
	public static final String NOTEQUALS = "notEquals";
	public static final String LIKE = "like";
	public static final String NOTLIKE = "notLike";
	public static final String ISNULL = "isNull";
}
