package com.cms.services.admin.common;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.modelmapper.PropertyMap;
import org.modelmapper.convention.MatchingStrategies;

/**
 * May 11, 2020
 * 
 * @author Babu Gali
 *
 */
public class ObjectMapperUtils {

	private static ModelMapper modelMapper = new ModelMapper();

	/**
	 * Model mapper property setting are specified in the following block. Default
	 * property matching strategy is set to Strict see {@link MatchingStrategies}
	 * Custom mappings are added using {@link ModelMapper#addMappings(PropertyMap)}
	 */
	static {
		modelMapper = new ModelMapper();
		modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
	}

	/**
	 * Hide from public usage.
	 */
	private ObjectMapperUtils() {
	}

	/**
	 * <p>
	 * Note: outClass object must have default constructor with no arguments
	 * </p>
	 *
	 * @param <D>      type of result object.
	 * @param <T>      type of source object to map from.
	 * @param entity   entity that needs to be mapped.
	 * @param outClass class of result object.
	 * @return new object of <code>outClass</code> type.
	 */
	public static <D, T> D map(final T entity, Class<D> outClass) {
		return modelMapper.map(entity, outClass);
	}

	/**
	 * <p>
	 * Note: outClass object must have default constructor with no arguments
	 * </p>
	 *
	 * @param entityList list of entities that needs to be mapped
	 * @param outCLass   class of result list element
	 * @param <D>        type of objects in result list
	 * @param <T>        type of entity in <code>entityList</code>
	 * @return list of mapped object with <code><D></code> type.
	 */
	public static <D, T> List<D> mapAll(final Collection<T> entityList, Class<D> outCLass) {
		return entityList.stream().map(entity -> map(entity, outCLass)).collect(Collectors.toList());
	}

	/**
	 * Maps {@code source} to {@code destination}.
	 *
	 * @param source      object to map from
	 * @param destination object to map to
	 */
	public static <S, D> D map(final S source, D destination) {
		modelMapper.map(source, destination);
		return destination;
	}

	/**
	 * @param <D>
	 * @param <T>
	 * @param entityList
	 * @param outCLass
	 * @param customModelMapper
	 * @return
	 */
	public static <D, T> List<D> mapAll(final Collection<T> entityList, Class<D> outCLass,
			PropertyMap<T, D> customModelMapperMap) {
		modelMapper.addMappings(customModelMapperMap); // TODO: check is it impacting other mappings, as modelMapper is
														// a global object
		return entityList.stream().map(entity -> map(entity, outCLass)).collect(Collectors.toList());
	}

	/**
	 * @param <D>
	 * @param <T>
	 * @param entity
	 * @param outClass
	 * @param customModelMapper
	 * @return
	 */
	public static <D, T> D map(final T entity, Class<D> outClass, PropertyMap<T, D> customModelMapperMap) {
		modelMapper.addMappings(customModelMapperMap); // TODO: check is it impacting other mappings, as modelMapper is
														// a global object
		return modelMapper.map(entity, outClass);
	}
}
