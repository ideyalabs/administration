/**
 * May 11, 2020
 * eLearner
 */
package com.cms.services.admin.common;

import java.util.Optional;

/**
 * May 11, 2020
 * 
 * @author Babu Gali
 *
 */
public class Utils {

	public static Boolean isValidNumber(Long number) {
		return Optional.ofNullable(number).orElse(0L) > 0;
	}
}
