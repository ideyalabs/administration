package com.cms.services.admin.common.exception;

/**
 * October 15, 2020
 * 
 * @author Sanjeev P
 *
 */
public class ResourceNotFoundException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7301803178164389763L;

	private Long resourceId;

	public ResourceNotFoundException(Long resourceId) {
		super();
		this.resourceId = resourceId;
	}

	/**
	 * @param message
	 * @param cause
	 * @param resourceId
	 */
	public ResourceNotFoundException(String message, Throwable cause, Long resourceId) {
		super(message, cause);
		this.resourceId = resourceId;

	}

	/**
	 * @param message
	 * @param resourceId
	 */
	public ResourceNotFoundException(String message, Long resourceId) {
		super(message);
		this.resourceId = resourceId;

	}

	/**
	 * @param cause
	 * @param resourceId
	 */
	public ResourceNotFoundException(Throwable cause, Long resourceId) {
		super(cause);
		this.resourceId = resourceId;

	}

	/**
	 * @return the resourceId
	 */
	public Long getResourceId() {
		return resourceId;
	}

	/**
	 * @param resourceId the resourceId to set
	 */
	public void setResourceId(Long resourceId) {
		this.resourceId = resourceId;
	}

}
