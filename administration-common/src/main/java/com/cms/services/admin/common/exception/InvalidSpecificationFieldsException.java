package com.cms.services.admin.common.exception;

/**
 * October 15, 2020
 * 
 * @author Sanjeev P
 *
 */
public class InvalidSpecificationFieldsException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7301803178164389763L;

	/**
	 * @param resourceId
	 */
	public InvalidSpecificationFieldsException() {
		super();
	}

	/**
	 * @param message
	 * @param cause
	 * @param resourceId
	 */
	public InvalidSpecificationFieldsException(String message, Throwable cause, Long resourceId) {
		super(message, cause);

	}

	/**
	 * @param message
	 * @param resourceId
	 */
	public InvalidSpecificationFieldsException(String message, Long resourceId) {
		super(message);

	}

	/**
	 * @param cause
	 * @param resourceId
	 */
	public InvalidSpecificationFieldsException(Throwable cause, Long resourceId) {
		super(cause);

	}

}
