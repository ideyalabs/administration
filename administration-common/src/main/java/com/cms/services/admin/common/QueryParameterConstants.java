package com.cms.services.admin.common;

/**
 * May 11, 2020
 * 
 * @author Babu Gali
 *
 */
public class QueryParameterConstants {

	public static final String SORT_DIRECTION = "sortDirection";
	public static final String SORT_COLUMN_NAME = "sortColumnName";
	public static final String FROM_DATE = "fromDate";
	public static final String TO_DATE = "toDate";
	public static final String DATE = "fromDate";

	public static final String ID = "id";
	public static final String FIRST_NAME = "firstName";

}
