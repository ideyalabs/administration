package com.cms.services.admin.service.vo;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * October 15, 2020
 * 
 * @author Sanjeev P
 *
 */
@Setter
@Getter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class ResponseVO {
	private Long code;
	private String message;
}
