package com.cms.services.admin.service.events;

import java.util.Date;

import org.springframework.data.domain.Pageable;

import com.ideyalabs.cms.services.datamodel.common.RoleEntity;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * May 11, 2020
 * 
 * @author Babu Gali
 *
 */
@Setter
@Getter
@Accessors(chain = true)
public class ReadAdminUserEvent {

	private Pageable pageable;
	private String sortDirection;
	private String sortColumnName;

	private Long id;
	private String name;
	private String emailId;	
	private String loginID;
	private RoleEntity role;
	private String password;
	private String confirmPassword;
	private Date lastAccessed;
	private Boolean status;

}
