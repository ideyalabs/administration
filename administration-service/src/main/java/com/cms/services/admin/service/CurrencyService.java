package com.cms.services.admin.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.cms.services.admin.common.ObjectMapperUtils;
import com.cms.services.admin.common.Utils;
import com.cms.services.admin.common.exception.InvalidSpecificationFieldsException;
import com.cms.services.admin.common.exception.ResourceNotFoundException;
import com.cms.services.admin.service.constants.Constants;
import com.cms.services.admin.service.events.CreateCurrencyEvent;
import com.cms.services.admin.service.events.PageReadEvent;
import com.cms.services.admin.service.events.ReadCurrencyEvent;
import com.cms.services.admin.service.events.ResourceCreatedEvent;
import com.cms.services.admin.service.events.ResourceDeletedEvent;
import com.cms.services.admin.service.vo.CurrencyVO;
import com.cms.services.admin.service.vo.ResponseVO;
import com.ideyalabs.cms.services.datamodel.common.CurrencyEntity;
import com.ideyalabs.cms.services.datamodel.repo.admin.CurrencyRepository;
import com.ideyalabs.cms.services.datamodel.specification.SpecificationBuilder;
import com.ideyalabs.cms.services.datamodel.specification.SpecificationFields;

/**
 * October 15, 2020
 * 
 * @author Sanjeev P
 *
 */
@Component
public interface CurrencyService {

	/**
	 * @param event
	 * @return
	 */
	public ResourceCreatedEvent saveOrUpdate(CreateCurrencyEvent event);

	/**
	 * @param request
	 * @return
	 */
	public PageReadEvent<CurrencyVO> viewAll(ReadCurrencyEvent request);

	/**
	 * @param request
	 * @return
	 */
	public CurrencyVO findById(ReadCurrencyEvent request);

	/**
	 * @param request
	 * @return
	 */
	public ResourceDeletedEvent deleteById(ReadCurrencyEvent request);

	@Service
	class Impl implements CurrencyService {

		@Autowired
		CurrencyRepository currencyReposioty;

		/**
		 * @param event
		 * @return
		 */
		@Override
		public ResourceCreatedEvent saveOrUpdate(CreateCurrencyEvent event) {
			ResponseVO responseVO = new ResponseVO();

			try {
				CurrencyVO CurrencyVO = event.getCurrencyVO();
				CurrencyEntity currencyEntiry = null;
				ModelMapper modelMapper = new ModelMapper();
				if (Utils.isValidNumber(CurrencyVO.getId())) {
					currencyEntiry = currencyReposioty.getOne(CurrencyVO.getId());
				} else {
					currencyEntiry = new CurrencyEntity();
				}

				currencyEntiry = modelMapper.map(CurrencyVO, CurrencyEntity.class);

				currencyReposioty.save(currencyEntiry);

				responseVO.setCode(Constants.ResponseMessages.CODE_200);
				responseVO.setMessage(Constants.ResponseMessages.MESSAGE_200);
			} catch (Exception e) {
				responseVO.setCode(Constants.ResponseMessages.CODE_500);
				responseVO.setMessage(Constants.ResponseMessages.MESSAGE_500);
				// TODO : Logger
			}
			return new ResourceCreatedEvent(responseVO);
		}

		/**
		 *
		 */
		@Override
		public CurrencyVO findById(ReadCurrencyEvent request) {
			Optional<CurrencyEntity> dbContent = currencyReposioty.findById(request.getId());
			if (!dbContent.isPresent()) {
				throw new ResourceNotFoundException("Resource Not exists", request.getId());
			}
			CurrencyVO CurrencyVO = ObjectMapperUtils.map(dbContent.get(), CurrencyVO.class);
			return CurrencyVO;
		}

		/**
		 *
		 */
		@Override
		@Transactional
		public ResourceDeletedEvent deleteById(ReadCurrencyEvent request) {
			ResponseVO responseVO = new ResponseVO();
			try {
				currencyReposioty.softDeleteById(request.getId());
				responseVO.setCode(Constants.ResponseMessages.CODE_200);
				responseVO.setMessage(Constants.ResponseMessages.MESSAGE_200);
			} catch (Exception e) {
				responseVO.setCode(Constants.ResponseMessages.CODE_500);
				responseVO.setMessage(Constants.ResponseMessages.MESSAGE_500);
			}
			return new ResourceDeletedEvent(responseVO);
		}

		/**
		 *
		 */
		@Override
		public PageReadEvent<CurrencyVO> viewAll(ReadCurrencyEvent request) {

			List<SpecificationFields> specList = new ArrayList<>();

			specList = new ArrayList<>();
			SpecificationFields isActive = new SpecificationFields();
			isActive.setColumName("deleted");
			isActive.setValue(Boolean.valueOf("false"));
			isActive.setConditionType(Constants.EQUALS);
			specList.add(isActive);

			SpecificationBuilder<CurrencyEntity> currencySpec;
			try {
				currencySpec = new SpecificationBuilder<CurrencyEntity>(specList);
			} catch (Exception e) {
				throw new InvalidSpecificationFieldsException();
			}

			Page<CurrencyEntity> dbContent = currencyReposioty.findAll(currencySpec,
					SpecificationBuilder.constructPageSpecification(request.getPageable().getPageNumber(),
							request.getPageable().getPageSize(), request.getSortColumnName(),
							request.getSortDirection()));
			List<CurrencyVO> content = ObjectMapperUtils.mapAll(dbContent.getContent(), CurrencyVO.class);
			Page<CurrencyVO> page = new PageImpl<>(content, request.getPageable(),
					dbContent != null ? dbContent.getTotalElements() : 0);
			return new PageReadEvent<>(page);
		}
	}

}
