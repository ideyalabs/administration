package com.cms.services.admin.service.events;

import com.cms.services.admin.service.vo.ResponseVO;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * October 15, 2020
 * 
 * @author Sanjeev P
 *
 */
@Setter
@Getter
@Builder
@Accessors(chain = true)
public class ResourceDeletedEvent {

	private ResponseVO responseVO;

	public ResourceDeletedEvent(ResponseVO responseVO) {
		this.responseVO = responseVO;
	}
}
