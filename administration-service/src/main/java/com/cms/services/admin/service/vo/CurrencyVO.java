package com.cms.services.admin.service.vo;

import javax.persistence.Column;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 * October 15, 2020
 * 
 * @author Sanjeev P
 *
 */
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
public class CurrencyVO {
	
	private Long id;

	private String name;

	private String description;

	private Integer currencyCode;
	
	private String wholeSymbol;
	
	private String wholePosition;
	
	private String fractionSymbol;
	
	private Long places;
	
	private boolean status;


}
