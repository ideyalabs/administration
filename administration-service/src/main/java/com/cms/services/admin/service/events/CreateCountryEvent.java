package com.cms.services.admin.service.events;



import com.cms.services.admin.service.vo.CountryVO;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * October 15, 2020
 * 
 * @author Sanjeev P
 *
 */
@Setter
@Getter
@Accessors(chain = true)
public class CreateCountryEvent {

	private CountryVO countryVO;

}
