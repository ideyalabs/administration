package com.cms.services.admin.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.cms.services.admin.common.ObjectMapperUtils;
import com.cms.services.admin.common.Utils;
import com.cms.services.admin.common.exception.InvalidSpecificationFieldsException;
import com.cms.services.admin.common.exception.ResourceNotFoundException;
import com.cms.services.admin.service.constants.Constants;
import com.cms.services.admin.service.events.CreateTaxEvent;
import com.cms.services.admin.service.events.PageReadEvent;
import com.cms.services.admin.service.events.ReadTaxEvent;
import com.cms.services.admin.service.events.ResourceCreatedEvent;
import com.cms.services.admin.service.events.ResourceDeletedEvent;
import com.cms.services.admin.service.vo.TaxVO;
import com.cms.services.admin.service.vo.ResponseVO;
import com.ideyalabs.cms.services.datamodel.admin.TaxEntity;
import com.ideyalabs.cms.services.datamodel.repo.admin.TaxRepository;
import com.ideyalabs.cms.services.datamodel.specification.SpecificationBuilder;
import com.ideyalabs.cms.services.datamodel.specification.SpecificationFields;

/**
 * October 15, 2020
 * 
 * @author Sanjeev P
 *
 */
@Component
public interface TaxService {

	/**
	 * @param event
	 * @return
	 */
	public ResourceCreatedEvent saveOrUpdate(CreateTaxEvent event);

	/**
	 * @param request
	 * @return
	 */
	public PageReadEvent<TaxVO> viewAll(ReadTaxEvent request);

	/**
	 * @param request
	 * @return
	 */
	public TaxVO findById(ReadTaxEvent request);

	/**
	 * @param request
	 * @return
	 */
	public ResourceDeletedEvent deleteById(ReadTaxEvent request);

	@Service
	class Impl implements TaxService {

		@Autowired
		TaxRepository taxReposioty;

		/**
		 * @param event
		 * @return
		 */
		@Override
		public ResourceCreatedEvent saveOrUpdate(CreateTaxEvent event) {
			ResponseVO responseVO = new ResponseVO();

			try {
				TaxVO taxVO = event.getTaxVO();
				TaxEntity taxEntiry = null;
				ModelMapper modelMapper = new ModelMapper();
				if (Utils.isValidNumber(taxVO.getId())) {
					taxEntiry = taxReposioty.getOne(taxVO.getId());
				} else {
					taxEntiry = new TaxEntity();
				}

				taxEntiry = modelMapper.map(taxVO, TaxEntity.class);

				taxReposioty.save(taxEntiry);

				responseVO.setCode(Constants.ResponseMessages.CODE_200);
				responseVO.setMessage(Constants.ResponseMessages.MESSAGE_200);
			} catch (Exception e) {
				responseVO.setCode(Constants.ResponseMessages.CODE_500);
				responseVO.setMessage(Constants.ResponseMessages.MESSAGE_500);
				// TODO : Logger
			}
			return new ResourceCreatedEvent(responseVO);
		}

		/**
		 *
		 */
		@Override
		public TaxVO findById(ReadTaxEvent request) {
			Optional<TaxEntity> dbContent = taxReposioty.findById(request.getId());
			if (!dbContent.isPresent()) {
				throw new ResourceNotFoundException("Resource Not exists", request.getId());
			}
			TaxVO TaxVO = ObjectMapperUtils.map(dbContent.get(), TaxVO.class);
			return TaxVO;
		}

		/**
		 *
		 */
		@Override
		@Transactional
		public ResourceDeletedEvent deleteById(ReadTaxEvent request) {
			ResponseVO responseVO = new ResponseVO();
			try {
				taxReposioty.softDeleteById(request.getId());
				responseVO.setCode(Constants.ResponseMessages.CODE_200);
				responseVO.setMessage(Constants.ResponseMessages.MESSAGE_200);
			} catch (Exception e) {
				responseVO.setCode(Constants.ResponseMessages.CODE_500);
				responseVO.setMessage(Constants.ResponseMessages.MESSAGE_500);
			}
			return new ResourceDeletedEvent(responseVO);
		}

		/**
		 *
		 */
		@Override
		public PageReadEvent<TaxVO> viewAll(ReadTaxEvent request) {

			List<SpecificationFields> specList = new ArrayList<>();

			specList = new ArrayList<>();
			SpecificationFields isActive = new SpecificationFields();
			isActive.setColumName("deleted");
			isActive.setValue(Boolean.valueOf("false"));
			isActive.setConditionType(Constants.EQUALS);
			specList.add(isActive);

			SpecificationBuilder<TaxEntity> taxSpec;
			try {
				taxSpec = new SpecificationBuilder<TaxEntity>(specList);
			} catch (Exception e) {
				throw new InvalidSpecificationFieldsException();
			}

			Page<TaxEntity> dbContent = taxReposioty.findAll(taxSpec,
					SpecificationBuilder.constructPageSpecification(request.getPageable().getPageNumber(),
							request.getPageable().getPageSize(), request.getSortColumnName(),
							request.getSortDirection()));
			List<TaxVO> content = ObjectMapperUtils.mapAll(dbContent.getContent(), TaxVO.class);
			Page<TaxVO> page = new PageImpl<>(content, request.getPageable(),
					dbContent != null ? dbContent.getTotalElements() : 0);
			return new PageReadEvent<>(page);
		}
	}

}
