package com.cms.services.admin.service.events;



import com.cms.services.admin.service.vo.AdminUserVO;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * October 16, 2020
 * 
 * @author Sanjeev P
 *
 */
@Setter
@Getter
@Accessors(chain = true)
public class CreateAdminUserEvent {

	private AdminUserVO adminUserVO;

}
