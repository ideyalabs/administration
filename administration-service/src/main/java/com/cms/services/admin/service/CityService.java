package com.cms.services.admin.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.cms.services.admin.common.ObjectMapperUtils;
import com.cms.services.admin.common.Utils;
import com.cms.services.admin.common.exception.InvalidSpecificationFieldsException;
import com.cms.services.admin.common.exception.ResourceNotFoundException;
import com.cms.services.admin.service.constants.Constants;
import com.cms.services.admin.service.events.CreateCityEvent;
import com.cms.services.admin.service.events.PageReadEvent;
import com.cms.services.admin.service.events.ReadCityEvent;
import com.cms.services.admin.service.events.ResourceCreatedEvent;
import com.cms.services.admin.service.events.ResourceDeletedEvent;
import com.cms.services.admin.service.vo.CityVO;
import com.cms.services.admin.service.vo.ResponseVO;
import com.ideyalabs.cms.services.datamodel.admin.CityEntity;
import com.ideyalabs.cms.services.datamodel.repo.admin.CityRepository;
import com.ideyalabs.cms.services.datamodel.specification.SpecificationBuilder;
import com.ideyalabs.cms.services.datamodel.specification.SpecificationFields;

/**
 * October 15, 2020
 * 
 * @author Sanjeev P
 *
 */
@Component
public interface CityService {

	/**
	 * @param event
	 * @return
	 */
	public ResourceCreatedEvent saveOrUpdate(CreateCityEvent event);

	/**
	 * @param request
	 * @return
	 */
	public PageReadEvent<CityVO> viewAll(ReadCityEvent request);

	/**
	 * @param request
	 * @return
	 */
	public CityVO findById(ReadCityEvent request);

	/**
	 * @param request
	 * @return
	 */
	public ResourceDeletedEvent deleteById(ReadCityEvent request);

	@Service
	class Impl implements CityService {

		@Autowired
		CityRepository cityReposioty;

		/**
		 * @param event
		 * @return
		 */
		@Override
		public ResourceCreatedEvent saveOrUpdate(CreateCityEvent event) {
			ResponseVO responseVO = new ResponseVO();

			try {
				CityVO cityVO = event.getCityVO();
				CityEntity subscriberEntiry = null;
				ModelMapper modelMapper = new ModelMapper();
				if (Utils.isValidNumber(cityVO.getId())) {
					subscriberEntiry = cityReposioty.getOne(cityVO.getId());
				} else {
					subscriberEntiry = new CityEntity();
				}

				subscriberEntiry = modelMapper.map(cityVO, CityEntity.class);

				cityReposioty.save(subscriberEntiry);

				responseVO.setCode(Constants.ResponseMessages.CODE_200);
				responseVO.setMessage(Constants.ResponseMessages.MESSAGE_200);
			} catch (Exception e) {
				responseVO.setCode(Constants.ResponseMessages.CODE_500);
				responseVO.setMessage(Constants.ResponseMessages.MESSAGE_500);
				// TODO : Logger
			}
			return new ResourceCreatedEvent(responseVO);
		}

		/**
		 *
		 */
		@Override
		public CityVO findById(ReadCityEvent request) {
			Optional<CityEntity> dbContent = cityReposioty.findById(request.getId());
			if (!dbContent.isPresent()) {
				throw new ResourceNotFoundException("Resource Not exists", request.getId());
			}
			CityVO cityVO = ObjectMapperUtils.map(dbContent.get(), CityVO.class);
			return cityVO;
		}

		/**
		 *
		 */
		@Override
		@Transactional
		public ResourceDeletedEvent deleteById(ReadCityEvent request) {
			ResponseVO responseVO = new ResponseVO();
			try {
				cityReposioty.softDeleteById(request.getId());
				responseVO.setCode(Constants.ResponseMessages.CODE_200);
				responseVO.setMessage(Constants.ResponseMessages.MESSAGE_200);
			} catch (Exception e) {
				responseVO.setCode(Constants.ResponseMessages.CODE_500);
				responseVO.setMessage(Constants.ResponseMessages.MESSAGE_500);
			}
			return new ResourceDeletedEvent(responseVO);
		}

		/**
		 *
		 */
		@Override
		public PageReadEvent<CityVO> viewAll(ReadCityEvent request) {

			List<SpecificationFields> specList = new ArrayList<>();

			specList = new ArrayList<>();
			SpecificationFields isActive = new SpecificationFields();
			isActive.setColumName("deleted");
			isActive.setValue(Boolean.valueOf("false"));
			isActive.setConditionType(Constants.EQUALS);
			specList.add(isActive);

			SpecificationBuilder<CityEntity> citySpec;
			try {
				citySpec = new SpecificationBuilder<CityEntity>(specList);
			} catch (Exception e) {
				throw new InvalidSpecificationFieldsException();
			}

			Page<CityEntity> dbContent = cityReposioty.findAll(citySpec,
					SpecificationBuilder.constructPageSpecification(request.getPageable().getPageNumber(),
							request.getPageable().getPageSize(), request.getSortColumnName(),
							request.getSortDirection()));
			List<CityVO> content = ObjectMapperUtils.mapAll(dbContent.getContent(), CityVO.class);
			Page<CityVO> page = new PageImpl<>(content, request.getPageable(),
					dbContent != null ? dbContent.getTotalElements() : 0);
			return new PageReadEvent<>(page);
		}
	}

}
