package com.cms.services.admin.service.constants;

/**
 * @author Sanjeev P
 *
 */
public class Constants {

	public static class ResponseMessages {
		public static final Long CODE_400 = 400L;
		public static final Long CODE_404 = 404L;
		public static final Long CODE_200 = 200L;
		public static final Long CODE_500 = 500L;

		public static final String MESSAGE_400 = "Already Existed";
		public static final String MESSAGE_200 = "Success";
		public static final String MESSAGE_500 = "Internal Server Error";
		public static final String MESSAGE_404 = "Not Existed";
	}

	public static final String EQUALS = null;

}
