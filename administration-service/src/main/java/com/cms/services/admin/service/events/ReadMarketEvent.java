package com.cms.services.admin.service.events;

import org.springframework.data.domain.Pageable;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * May 11, 2020
 * 
 * @author Babu Gali
 *
 */
@Setter
@Getter
@Accessors(chain = true)
public class ReadMarketEvent {

	private Pageable pageable;
	private String sortDirection;
	private String sortColumnName;

	private Long id;
	private String title;
	private String currency;
	private boolean taxInPrice;
	private boolean status;

}
