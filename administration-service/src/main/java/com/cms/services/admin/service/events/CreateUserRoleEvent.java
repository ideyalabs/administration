package com.cms.services.admin.service.events;



import com.cms.services.admin.service.vo.UserRoleVO;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * October 16, 2020
 * 
 * @author Sanjeev P
 *
 */
@Setter
@Getter
@Accessors(chain = true)
public class CreateUserRoleEvent {

	private UserRoleVO userRoleVO;

}
