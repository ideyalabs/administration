package com.cms.services.admin.service.vo;

import java.util.Date;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 * October 15, 2020
 * 
 * @author Sanjeev P
 *
 */
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
public class MarketVO {
	
	private Long id;
	private String title;
	private String currency;
	private boolean taxInPrice;
	private boolean status;
	//private List<CountryVO> country;
	private String language;
	private Date createdOn;
	private Date updatedOn;

}
