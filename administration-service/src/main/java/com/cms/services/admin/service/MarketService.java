package com.cms.services.admin.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.cms.services.admin.common.ObjectMapperUtils;
import com.cms.services.admin.common.Utils;
import com.cms.services.admin.common.exception.InvalidSpecificationFieldsException;
import com.cms.services.admin.common.exception.ResourceNotFoundException;
import com.cms.services.admin.service.constants.Constants;
import com.cms.services.admin.service.events.CreateMarketEvent;
import com.cms.services.admin.service.events.PageReadEvent;
import com.cms.services.admin.service.events.ReadMarketEvent;
import com.cms.services.admin.service.events.ResourceCreatedEvent;
import com.cms.services.admin.service.events.ResourceDeletedEvent;
import com.cms.services.admin.service.vo.MarketVO;
import com.cms.services.admin.service.vo.ResponseVO;
import com.ideyalabs.cms.services.datamodel.admin.MarketEntity;
import com.ideyalabs.cms.services.datamodel.repo.admin.MarketRepository;
import com.ideyalabs.cms.services.datamodel.specification.SpecificationBuilder;
import com.ideyalabs.cms.services.datamodel.specification.SpecificationFields;

/**
 * October 15, 2020
 * 
 * @author Sanjeev P
 *
 */
@Component
public interface MarketService {

	/**
	 * @param event
	 * @return
	 */
	public ResourceCreatedEvent saveOrUpdate(CreateMarketEvent event);

	/**
	 * @param request
	 * @return
	 */
	public PageReadEvent<MarketVO> viewAll(ReadMarketEvent request);

	/**
	 * @param request
	 * @return
	 */
	public MarketVO findById(ReadMarketEvent request);

	/**
	 * @param request
	 * @return
	 */
	public ResourceDeletedEvent deleteById(ReadMarketEvent request);

	@Service
	class Impl implements MarketService {

		@Autowired
		MarketRepository MarketReposioty;

		/**
		 * @param event
		 * @return
		 */
		@Override
		public ResourceCreatedEvent saveOrUpdate(CreateMarketEvent event) {
			ResponseVO responseVO = new ResponseVO();

			try {
				MarketVO MarketVO = event.getMarketVO();
				MarketEntity marketEntiry = null;
				ModelMapper modelMapper = new ModelMapper();
				if (Utils.isValidNumber(MarketVO.getId())) {
					marketEntiry = MarketReposioty.getOne(MarketVO.getId());
				} else {
					marketEntiry = new MarketEntity();
				}

				marketEntiry = modelMapper.map(MarketVO, MarketEntity.class);

				MarketReposioty.save(marketEntiry);

				responseVO.setCode(Constants.ResponseMessages.CODE_200);
				responseVO.setMessage(Constants.ResponseMessages.MESSAGE_200);
			} catch (Exception e) {
				responseVO.setCode(Constants.ResponseMessages.CODE_500);
				responseVO.setMessage(Constants.ResponseMessages.MESSAGE_500);
				// TODO : Logger
			}
			return new ResourceCreatedEvent(responseVO);
		}

		/**
		 *
		 */
		@Override
		public MarketVO findById(ReadMarketEvent request) {
			Optional<MarketEntity> dbContent = MarketReposioty.findById(request.getId());
			if (!dbContent.isPresent()) {
				throw new ResourceNotFoundException("Resource Not exists", request.getId());
			}
			MarketVO MarketVO = ObjectMapperUtils.map(dbContent.get(), MarketVO.class);
			return MarketVO;
		}

		/**
		 *
		 */
		@Override
		@Transactional
		public ResourceDeletedEvent deleteById(ReadMarketEvent request) {
			ResponseVO responseVO = new ResponseVO();
			try {
				MarketReposioty.softDeleteById(request.getId());
				responseVO.setCode(Constants.ResponseMessages.CODE_200);
				responseVO.setMessage(Constants.ResponseMessages.MESSAGE_200);
			} catch (Exception e) {
				responseVO.setCode(Constants.ResponseMessages.CODE_500);
				responseVO.setMessage(Constants.ResponseMessages.MESSAGE_500);
			}
			return new ResourceDeletedEvent(responseVO);
		}

		/**
		 *
		 */
		@Override
		public PageReadEvent<MarketVO> viewAll(ReadMarketEvent request) {

			List<SpecificationFields> specList = new ArrayList<>();

			specList = new ArrayList<>();
			SpecificationFields isActive = new SpecificationFields();
			isActive.setColumName("deleted");
			isActive.setValue(Boolean.valueOf("false"));
			isActive.setConditionType(Constants.EQUALS);
			specList.add(isActive);

			SpecificationBuilder<MarketEntity> marketSpec;
			try {
				marketSpec = new SpecificationBuilder<MarketEntity>(specList);
			} catch (Exception e) {
				throw new InvalidSpecificationFieldsException();
			}

			Page<MarketEntity> dbContent = MarketReposioty.findAll(marketSpec,
					SpecificationBuilder.constructPageSpecification(request.getPageable().getPageNumber(),
							request.getPageable().getPageSize(), request.getSortColumnName(),
							request.getSortDirection()));
			List<MarketVO> content = ObjectMapperUtils.mapAll(dbContent.getContent(), MarketVO.class);
			Page<MarketVO> page = new PageImpl<>(content, request.getPageable(),
					dbContent != null ? dbContent.getTotalElements() : 0);
			return new PageReadEvent<>(page);
		}
	}

}
