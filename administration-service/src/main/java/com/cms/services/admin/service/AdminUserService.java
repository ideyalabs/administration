package com.cms.services.admin.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.cms.services.admin.common.ObjectMapperUtils;
import com.cms.services.admin.common.Utils;
import com.cms.services.admin.common.exception.InvalidSpecificationFieldsException;
import com.cms.services.admin.common.exception.ResourceNotFoundException;
import com.cms.services.admin.service.constants.Constants;
import com.cms.services.admin.service.events.CreateAdminUserEvent;
import com.cms.services.admin.service.events.PageReadEvent;
import com.cms.services.admin.service.events.ReadAdminUserEvent;
import com.cms.services.admin.service.events.ResourceCreatedEvent;
import com.cms.services.admin.service.events.ResourceDeletedEvent;
import com.cms.services.admin.service.vo.AdminUserVO;
import com.cms.services.admin.service.vo.ResponseVO;
import com.ideyalabs.cms.services.datamodel.admin.AdminUserEntity;
import com.ideyalabs.cms.services.datamodel.repo.admin.AdminUserRepository;
import com.ideyalabs.cms.services.datamodel.specification.SpecificationBuilder;
import com.ideyalabs.cms.services.datamodel.specification.SpecificationFields;

/**
 * October 15, 2020
 * 
 * @author Sanjeev P
 *
 */
@Component
public interface AdminUserService {

	/**
	 * @param event
	 * @return
	 */
	public ResourceCreatedEvent saveOrUpdate(CreateAdminUserEvent event);

	/**
	 * @param request
	 * @return
	 */
	public PageReadEvent<AdminUserVO> viewAll(ReadAdminUserEvent request);

	/**
	 * @param request
	 * @return
	 */
	public AdminUserVO findById(ReadAdminUserEvent request);

	/**
	 * @param request
	 * @return
	 */
	public ResourceDeletedEvent deleteById(ReadAdminUserEvent request);

	@Service
	class Impl implements AdminUserService {

		@Autowired
		AdminUserRepository adminUserReposioty;

		/**
		 * @param event
		 * @return
		 */
		@Override
		public ResourceCreatedEvent saveOrUpdate(CreateAdminUserEvent event) {
			ResponseVO responseVO = new ResponseVO();

			try {
				AdminUserVO AdminUserVO = event.getAdminUserVO();
				AdminUserEntity subscriberEntiry = null;
				ModelMapper modelMapper = new ModelMapper();
				if (Utils.isValidNumber(AdminUserVO.getId())) {
					subscriberEntiry = adminUserReposioty.getOne(AdminUserVO.getId());
				} else {
					subscriberEntiry = new AdminUserEntity();
				}

				subscriberEntiry = modelMapper.map(AdminUserVO, AdminUserEntity.class);

				adminUserReposioty.save(subscriberEntiry);

				responseVO.setCode(Constants.ResponseMessages.CODE_200);
				responseVO.setMessage(Constants.ResponseMessages.MESSAGE_200);
			} catch (Exception e) {
				responseVO.setCode(Constants.ResponseMessages.CODE_500);
				responseVO.setMessage(Constants.ResponseMessages.MESSAGE_500);
				// TODO : Logger
			}
			return new ResourceCreatedEvent(responseVO);
		}

		/**
		 *
		 */
		@Override
		public AdminUserVO findById(ReadAdminUserEvent request) {
			Optional<AdminUserEntity> dbContent = adminUserReposioty.findById(request.getId());
			if (!dbContent.isPresent()) {
				throw new ResourceNotFoundException("Resource Not exists", request.getId());
			}
			AdminUserVO AdminUserVO = ObjectMapperUtils.map(dbContent.get(), AdminUserVO.class);
			return AdminUserVO;
		}

		/**
		 *
		 */
		@Override
		@Transactional
		public ResourceDeletedEvent deleteById(ReadAdminUserEvent request) {
			ResponseVO responseVO = new ResponseVO();
			try {
				adminUserReposioty.softDeleteById(request.getId());
				responseVO.setCode(Constants.ResponseMessages.CODE_200);
				responseVO.setMessage(Constants.ResponseMessages.MESSAGE_200);
			} catch (Exception e) {
				responseVO.setCode(Constants.ResponseMessages.CODE_500);
				responseVO.setMessage(Constants.ResponseMessages.MESSAGE_500);
			}
			return new ResourceDeletedEvent(responseVO);
		}

		/**
		 *
		 */
		@Override
		public PageReadEvent<AdminUserVO> viewAll(ReadAdminUserEvent request) {

			List<SpecificationFields> specList = new ArrayList<>();

			specList = new ArrayList<>();
			SpecificationFields isActive = new SpecificationFields();
			isActive.setColumName("deleted");
			isActive.setValue(Boolean.valueOf("false"));
			isActive.setConditionType(Constants.EQUALS);
			specList.add(isActive);

			SpecificationBuilder<AdminUserEntity> AdminUserSpec;
			try {
				AdminUserSpec = new SpecificationBuilder<AdminUserEntity>(specList);
			} catch (Exception e) {
				throw new InvalidSpecificationFieldsException();
			}

			Page<AdminUserEntity> dbContent = adminUserReposioty.findAll(AdminUserSpec,
					SpecificationBuilder.constructPageSpecification(request.getPageable().getPageNumber(),
							request.getPageable().getPageSize(), request.getSortColumnName(),
							request.getSortDirection()));
			List<AdminUserVO> content = ObjectMapperUtils.mapAll(dbContent.getContent(), AdminUserVO.class);
			Page<AdminUserVO> page = new PageImpl<>(content, request.getPageable(),
					dbContent != null ? dbContent.getTotalElements() : 0);
			return new PageReadEvent<>(page);
		}
	}

}
