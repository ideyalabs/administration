package com.cms.services.admin.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.cms.services.admin.common.ObjectMapperUtils;
import com.cms.services.admin.common.Utils;
import com.cms.services.admin.common.exception.InvalidSpecificationFieldsException;
import com.cms.services.admin.common.exception.ResourceNotFoundException;
import com.cms.services.admin.service.constants.Constants;
import com.cms.services.admin.service.events.CreateAccessControlEvent;
import com.cms.services.admin.service.events.PageReadEvent;
import com.cms.services.admin.service.events.ReadAccessControlEvent;
import com.cms.services.admin.service.events.ResourceCreatedEvent;
import com.cms.services.admin.service.events.ResourceDeletedEvent;
import com.cms.services.admin.service.vo.AccessControlVO;
import com.cms.services.admin.service.vo.ResponseVO;
import com.ideyalabs.cms.services.datamodel.admin.AccessControlEntity;
import com.ideyalabs.cms.services.datamodel.repo.admin.AccessControlRepository;
import com.ideyalabs.cms.services.datamodel.specification.SpecificationBuilder;
import com.ideyalabs.cms.services.datamodel.specification.SpecificationFields;

/**
 * October 15, 2020
 * 
 * @author Sanjeev P
 *
 */
@Component
public interface AccessControlService {

	/**
	 * @param event
	 * @return
	 */
	public ResourceCreatedEvent saveOrUpdate(CreateAccessControlEvent event);

	/**
	 * @param request
	 * @return
	 */
	public PageReadEvent<AccessControlVO> viewAll(ReadAccessControlEvent request);

	/**
	 * @param request
	 * @return
	 */
	public AccessControlVO findById(ReadAccessControlEvent request);

	/**
	 * @param request
	 * @return
	 */
	public ResourceDeletedEvent deleteById(ReadAccessControlEvent request);

	@Service
	class Impl implements AccessControlService {

		@Autowired
		AccessControlRepository AccessControlReposioty;

		/**
		 * @param event
		 * @return
		 */
		@Override
		public ResourceCreatedEvent saveOrUpdate(CreateAccessControlEvent event) {
			ResponseVO responseVO = new ResponseVO();

			try {
				AccessControlVO AccessControlVO = event.getAccessControlVO();
				AccessControlEntity subscriberEntiry = null;
				ModelMapper modelMapper = new ModelMapper();
				if (Utils.isValidNumber(AccessControlVO.getId())) {
					subscriberEntiry = AccessControlReposioty.getOne(AccessControlVO.getId());
				} else {
					subscriberEntiry = new AccessControlEntity();
				}

				subscriberEntiry = modelMapper.map(AccessControlVO, AccessControlEntity.class);

				AccessControlReposioty.save(subscriberEntiry);

				responseVO.setCode(Constants.ResponseMessages.CODE_200);
				responseVO.setMessage(Constants.ResponseMessages.MESSAGE_200);
			} catch (Exception e) {
				responseVO.setCode(Constants.ResponseMessages.CODE_500);
				responseVO.setMessage(Constants.ResponseMessages.MESSAGE_500);
				// TODO : Logger
			}
			return new ResourceCreatedEvent(responseVO);
		}

		/**
		 *
		 */
		@Override
		public AccessControlVO findById(ReadAccessControlEvent request) {
			Optional<AccessControlEntity> dbContent = AccessControlReposioty.findById(request.getId());
			if (!dbContent.isPresent()) {
				throw new ResourceNotFoundException("Resource Not exists", request.getId());
			}
			AccessControlVO AccessControlVO = ObjectMapperUtils.map(dbContent.get(), AccessControlVO.class);
			return AccessControlVO;
		}

		/**
		 *
		 */
		@Override
		@Transactional
		public ResourceDeletedEvent deleteById(ReadAccessControlEvent request) {
			ResponseVO responseVO = new ResponseVO();
			try {
				AccessControlReposioty.softDeleteById(request.getId());
				responseVO.setCode(Constants.ResponseMessages.CODE_200);
				responseVO.setMessage(Constants.ResponseMessages.MESSAGE_200);
			} catch (Exception e) {
				responseVO.setCode(Constants.ResponseMessages.CODE_500);
				responseVO.setMessage(Constants.ResponseMessages.MESSAGE_500);
			}
			return new ResourceDeletedEvent(responseVO);
		}

		/**
		 *
		 */
		@Override
		public PageReadEvent<AccessControlVO> viewAll(ReadAccessControlEvent request) {

			List<SpecificationFields> specList = new ArrayList<>();

			specList = new ArrayList<>();
			SpecificationFields isActive = new SpecificationFields();
			isActive.setColumName("deleted");
			isActive.setValue(Boolean.valueOf("false"));
			isActive.setConditionType(Constants.EQUALS);
			specList.add(isActive);

			SpecificationBuilder<AccessControlEntity> AccessControlSpec;
			try {
				AccessControlSpec = new SpecificationBuilder<AccessControlEntity>(specList);
			} catch (Exception e) {
				throw new InvalidSpecificationFieldsException();
			}

			Page<AccessControlEntity> dbContent = AccessControlReposioty.findAll(AccessControlSpec,
					SpecificationBuilder.constructPageSpecification(request.getPageable().getPageNumber(),
							request.getPageable().getPageSize(), request.getSortColumnName(),
							request.getSortDirection()));
			List<AccessControlVO> content = ObjectMapperUtils.mapAll(dbContent.getContent(), AccessControlVO.class);
			Page<AccessControlVO> page = new PageImpl<>(content, request.getPageable(),
					dbContent != null ? dbContent.getTotalElements() : 0);
			return new PageReadEvent<>(page);
		}
	}

}
