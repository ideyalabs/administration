package com.cms.services.admin.service.events;

import org.springframework.data.domain.Pageable;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * May 11, 2020
 * 
 * @author Babu Gali
 *
 */
@Setter
@Getter
@Accessors(chain = true)
public class ReadCountryEvent {

	private Pageable pageable;
	private String sortDirection;
	private String sortColumnName;

	private Long id;
	private String name;
	private String countryCode;
	private String codeNumber;
	private String coordinates;
	private boolean status;

}
