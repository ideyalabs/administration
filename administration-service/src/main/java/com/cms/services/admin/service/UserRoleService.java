package com.cms.services.admin.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.cms.services.admin.common.ObjectMapperUtils;
import com.cms.services.admin.common.Utils;
import com.cms.services.admin.common.exception.InvalidSpecificationFieldsException;
import com.cms.services.admin.common.exception.ResourceNotFoundException;
import com.cms.services.admin.service.constants.Constants;
import com.cms.services.admin.service.events.CreateUserRoleEvent;
import com.cms.services.admin.service.events.PageReadEvent;
import com.cms.services.admin.service.events.ReadUserRoleEvent;
import com.cms.services.admin.service.events.ResourceCreatedEvent;
import com.cms.services.admin.service.events.ResourceDeletedEvent;
import com.cms.services.admin.service.vo.ResponseVO;
import com.cms.services.admin.service.vo.UserRoleVO;
import com.ideyalabs.cms.services.datamodel.common.RoleEntity;
import com.ideyalabs.cms.services.datamodel.repo.admin.UserRoleRepository;
import com.ideyalabs.cms.services.datamodel.specification.SpecificationBuilder;
import com.ideyalabs.cms.services.datamodel.specification.SpecificationFields;

/**
 * October 15, 2020
 * 
 * @author Sanjeev P
 *
 */
@Component
public interface UserRoleService {

	/**
	 * @param event
	 * @return
	 */
	public ResourceCreatedEvent saveOrUpdate(CreateUserRoleEvent event);

	/**
	 * @param request
	 * @return
	 */
	public PageReadEvent<UserRoleVO> viewAll(ReadUserRoleEvent request);

	/**
	 * @param request
	 * @return
	 */
	public UserRoleVO findById(ReadUserRoleEvent request);

	/**
	 * @param request
	 * @return
	 */
	public ResourceDeletedEvent deleteById(ReadUserRoleEvent request);

	@Service
	class Impl implements UserRoleService {

		@Autowired
		UserRoleRepository UserRoleReposioty;

		/**
		 * @param event
		 * @return
		 */
		@Override
		public ResourceCreatedEvent saveOrUpdate(CreateUserRoleEvent event) {
			ResponseVO responseVO = new ResponseVO();

			try {
				UserRoleVO UserRoleVO = event.getUserRoleVO();
				RoleEntity subscriberEntiry = null;
				ModelMapper modelMapper = new ModelMapper();
				if (Utils.isValidNumber(UserRoleVO.getId())) {
					subscriberEntiry = UserRoleReposioty.getOne(UserRoleVO.getId());
				} else {
					subscriberEntiry = new RoleEntity();
				}

				subscriberEntiry = modelMapper.map(UserRoleVO, RoleEntity.class);

				UserRoleReposioty.save(subscriberEntiry);

				responseVO.setCode(Constants.ResponseMessages.CODE_200);
				responseVO.setMessage(Constants.ResponseMessages.MESSAGE_200);
			} catch (Exception e) {
				responseVO.setCode(Constants.ResponseMessages.CODE_500);
				responseVO.setMessage(Constants.ResponseMessages.MESSAGE_500);
				// TODO : Logger
			}
			return new ResourceCreatedEvent(responseVO);
		}

		/**
		 *
		 */
		@Override
		public UserRoleVO findById(ReadUserRoleEvent request) {
			Optional<RoleEntity> dbContent = UserRoleReposioty.findById(request.getId());
			if (!dbContent.isPresent()) {
				throw new ResourceNotFoundException("Resource Not exists", request.getId());
			}
			UserRoleVO UserRoleVO = ObjectMapperUtils.map(dbContent.get(), UserRoleVO.class);
			return UserRoleVO;
		}

		/**
		 *
		 */
		@Override
		@Transactional
		public ResourceDeletedEvent deleteById(ReadUserRoleEvent request) {
			ResponseVO responseVO = new ResponseVO();
			try {
				UserRoleReposioty.softDeleteById(request.getId());
				responseVO.setCode(Constants.ResponseMessages.CODE_200);
				responseVO.setMessage(Constants.ResponseMessages.MESSAGE_200);
			} catch (Exception e) {
				responseVO.setCode(Constants.ResponseMessages.CODE_500);
				responseVO.setMessage(Constants.ResponseMessages.MESSAGE_500);
			}
			return new ResourceDeletedEvent(responseVO);
		}

		/**
		 *
		 */
		@Override
		public PageReadEvent<UserRoleVO> viewAll(ReadUserRoleEvent request) {

			List<SpecificationFields> specList = new ArrayList<>();

			specList = new ArrayList<>();
			SpecificationFields isActive = new SpecificationFields();
			isActive.setColumName("deleted");
			isActive.setValue(Boolean.valueOf("false"));
			isActive.setConditionType(Constants.EQUALS);
			specList.add(isActive);

			SpecificationBuilder<RoleEntity> UserRoleSpec;
			try {
				UserRoleSpec = new SpecificationBuilder<RoleEntity>(specList);
			} catch (Exception e) {
				throw new InvalidSpecificationFieldsException();
			}

			Page<RoleEntity> dbContent = UserRoleReposioty.findAll(UserRoleSpec,
					SpecificationBuilder.constructPageSpecification(request.getPageable().getPageNumber(),
							request.getPageable().getPageSize(), request.getSortColumnName(),
							request.getSortDirection()));
			List<UserRoleVO> content = ObjectMapperUtils.mapAll(dbContent.getContent(), UserRoleVO.class);
			Page<UserRoleVO> page = new PageImpl<>(content, request.getPageable(),
					dbContent != null ? dbContent.getTotalElements() : 0);
			return new PageReadEvent<>(page);
		}
	}

}
