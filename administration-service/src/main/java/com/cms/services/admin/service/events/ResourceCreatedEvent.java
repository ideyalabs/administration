package com.cms.services.admin.service.events;

import com.cms.services.admin.service.vo.ResponseVO;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * May 11, 2020
 * 
 * @author Babu Gali
 *
 */
@Setter
@Getter
@Builder
@Accessors(chain = true)
public class ResourceCreatedEvent 
{

	private ResponseVO responseVO;
	public ResourceCreatedEvent(ResponseVO responseVO) {
		this.responseVO=responseVO;		
	}
}
