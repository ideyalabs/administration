package com.cms.services.admin.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.cms.services.admin.common.ObjectMapperUtils;
import com.cms.services.admin.common.Utils;
import com.cms.services.admin.common.exception.InvalidSpecificationFieldsException;
import com.cms.services.admin.common.exception.ResourceNotFoundException;
import com.cms.services.admin.service.constants.Constants;
import com.cms.services.admin.service.events.CreateLanguageEvent;
import com.cms.services.admin.service.events.PageReadEvent;
import com.cms.services.admin.service.events.ReadLanguageEvent;
import com.cms.services.admin.service.events.ResourceCreatedEvent;
import com.cms.services.admin.service.events.ResourceDeletedEvent;
import com.cms.services.admin.service.vo.LanguageVO;
import com.cms.services.admin.service.vo.ResponseVO;
import com.ideyalabs.cms.services.datamodel.common.LanguageEntity;
import com.ideyalabs.cms.services.datamodel.repo.admin.LanguageRepository;
import com.ideyalabs.cms.services.datamodel.specification.SpecificationBuilder;
import com.ideyalabs.cms.services.datamodel.specification.SpecificationFields;

/**
 * October 15, 2020
 * 
 * @author Sanjeev P
 *
 */
@Component
public interface LanguageService {

	/**
	 * @param event
	 * @return
	 */
	public ResourceCreatedEvent saveOrUpdate(CreateLanguageEvent event);

	/**
	 * @param request
	 * @return
	 */
	public PageReadEvent<LanguageVO> viewAll(ReadLanguageEvent request);

	/**
	 * @param request
	 * @return
	 */
	public LanguageVO findById(ReadLanguageEvent request);

	/**
	 * @param request
	 * @return
	 */
	public ResourceDeletedEvent deleteById(ReadLanguageEvent request);

	@Service
	class Impl implements LanguageService {

		@Autowired
		LanguageRepository languageReposioty;

		/**
		 * @param event
		 * @return
		 */
		@Override
		public ResourceCreatedEvent saveOrUpdate(CreateLanguageEvent event) {
			ResponseVO responseVO = new ResponseVO();

			try {
				LanguageVO languageVO = event.getLanguageVO();
				LanguageEntity languageEntiry = null;
				ModelMapper modelMapper = new ModelMapper();
				if (Utils.isValidNumber(languageVO.getId())) {
					languageEntiry = languageReposioty.getOne(languageVO.getId());
				} else {
					languageEntiry = new LanguageEntity();
				}

				languageEntiry = modelMapper.map(languageVO, LanguageEntity.class);

				languageReposioty.save(languageEntiry);

				responseVO.setCode(Constants.ResponseMessages.CODE_200);
				responseVO.setMessage(Constants.ResponseMessages.MESSAGE_200);
			} catch (Exception e) {
				responseVO.setCode(Constants.ResponseMessages.CODE_500);
				responseVO.setMessage(Constants.ResponseMessages.MESSAGE_500);
				// TODO : Logger
			}
			return new ResourceCreatedEvent(responseVO);
		}

		/**
		 *
		 */
		@Override
		public LanguageVO findById(ReadLanguageEvent request) {
			Optional<LanguageEntity> dbContent = languageReposioty.findById(request.getId());
			if (!dbContent.isPresent()) {
				throw new ResourceNotFoundException("Resource Not exists", request.getId());
			}
			LanguageVO languageVO = ObjectMapperUtils.map(dbContent.get(), LanguageVO.class);
			return languageVO;
		}

		/**
		 *
		 */
		@Override
		@Transactional
		public ResourceDeletedEvent deleteById(ReadLanguageEvent request) {
			ResponseVO responseVO = new ResponseVO();
			try {
				languageReposioty.softDeleteById(request.getId());
				responseVO.setCode(Constants.ResponseMessages.CODE_200);
				responseVO.setMessage(Constants.ResponseMessages.MESSAGE_200);
			} catch (Exception e) {
				responseVO.setCode(Constants.ResponseMessages.CODE_500);
				responseVO.setMessage(Constants.ResponseMessages.MESSAGE_500);
			}
			return new ResourceDeletedEvent(responseVO);
		}

		/**
		 *
		 */
		@Override
		public PageReadEvent<LanguageVO> viewAll(ReadLanguageEvent request) {

			List<SpecificationFields> specList = new ArrayList<>();

			specList = new ArrayList<>();
			SpecificationFields isActive = new SpecificationFields();
			isActive.setColumName("deleted");
			isActive.setValue(Boolean.valueOf(false));
			isActive.setConditionType(Constants.EQUALS);
			specList.add(isActive);

			SpecificationBuilder<LanguageEntity> languageSpec;
			try {
				languageSpec = new SpecificationBuilder<LanguageEntity>(specList);
			} catch (Exception e) {
				throw new InvalidSpecificationFieldsException();
			}

			Page<LanguageEntity> dbContent = languageReposioty.findAll(languageSpec,
					SpecificationBuilder.constructPageSpecification(request.getPageable().getPageNumber(),
							request.getPageable().getPageSize(), request.getSortColumnName(),
							request.getSortDirection()));
			List<LanguageVO> content = ObjectMapperUtils.mapAll(dbContent.getContent(), LanguageVO.class);
			Page<LanguageVO> page = new PageImpl<>(content, request.getPageable(),
					dbContent != null ? dbContent.getTotalElements() : 0);
			return new PageReadEvent<>(page);
		}
	}

}
