package com.ideyalabs.cms.services.admin.app.command.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.cms.services.admin.service.LanguageService;
import com.cms.services.admin.service.events.CreateLanguageEvent;
import com.cms.services.admin.service.events.ReadLanguageEvent;
import com.cms.services.admin.service.events.ResourceCreatedEvent;
import com.cms.services.admin.service.events.ResourceDeletedEvent;
import com.cms.services.admin.service.vo.LanguageVO;
import com.ideyalabs.cms.services.admin.app.resource.CreatedResponseModel;
import com.ideyalabs.cms.services.admin.app.resource.LanguageModel;
import com.ideyalabs.cms.services.admin.assembler.CreatedResponseResourceAssembler;
import com.ideyalabs.cms.services.admin.assembler.LanguageResourceAssembler;

import io.swagger.annotations.ApiOperation;
/**
 * 
 * @author sanjeev P
 *
 */
@RestController("/v1/language")
public class LanguageCommandController {

	@Autowired
	private LanguageService languageService;
	
	@Autowired
	private LanguageResourceAssembler assembler;
	

	@Autowired
	private CreatedResponseResourceAssembler responseAssembler;

	/**
	 * @param resource
	 * @param request
	 * @return
	 */
	@ApiOperation(value = "Create or Update Language", response = CreatedResponseModel.class)
	@PostMapping(value = "createLanguage", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@PutMapping(value = "createLanguage", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<CreatedResponseModel> saveOrUpdate(@RequestBody LanguageModel resource,
			HttpServletRequest request) {
		LanguageVO languageVO = assembler.fromModel(resource);
		CreateLanguageEvent event = new CreateLanguageEvent();
		event.setLanguageVO(languageVO);
		ResourceCreatedEvent resourceCreatedEvent = languageService.saveOrUpdate(event);
		CreatedResponseModel createdResponseModel = responseAssembler.toModel(resourceCreatedEvent.getResponseVO());
		return new ResponseEntity<CreatedResponseModel>(createdResponseModel, HttpStatus.OK);
	}

	/**
	 * @param cityid
	 * @return
	 */
	@ApiOperation(value = "Language City", response = CreatedResponseModel.class)
	@DeleteMapping(path = "/deleteLanguage/{languageid}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<CreatedResponseModel> deleteById(@PathVariable("languageid") Long languageId) {
		ReadLanguageEvent request = new ReadLanguageEvent();
		request.setId(languageId);
		ResourceDeletedEvent resourceDeletedEvent = languageService.deleteById(request);
		CreatedResponseModel createdResponseModel = responseAssembler.toModel(resourceDeletedEvent.getResponseVO());
		return new ResponseEntity<CreatedResponseModel>(createdResponseModel, HttpStatus.OK);
	}

}
