package com.ideyalabs.cms.services.admin.app.command.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
/**
 * 
 * @author sanjeev P
 *
 */
@RestController
public class HealthCheckController {

	@GetMapping("/healthCheck")
	public String checkHealth() {
		return "healthcheck";
	}
	
}
