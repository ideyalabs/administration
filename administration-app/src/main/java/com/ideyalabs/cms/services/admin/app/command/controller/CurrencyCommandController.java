package com.ideyalabs.cms.services.admin.app.command.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.cms.services.admin.service.CurrencyService;
import com.cms.services.admin.service.events.CreateCurrencyEvent;
import com.cms.services.admin.service.events.ReadCurrencyEvent;
import com.cms.services.admin.service.events.ResourceCreatedEvent;
import com.cms.services.admin.service.events.ResourceDeletedEvent;
import com.cms.services.admin.service.vo.CurrencyVO;
import com.ideyalabs.cms.services.admin.app.resource.CurrencyModel;
import com.ideyalabs.cms.services.admin.app.resource.CreatedResponseModel;
import com.ideyalabs.cms.services.admin.assembler.CurrencyResourceAssembler;
import com.ideyalabs.cms.services.admin.assembler.CreatedResponseResourceAssembler;

import io.swagger.annotations.ApiOperation;
/**
 * 
 * @author sanjeev P
 *
 */
@RestController("/v1/currency")
public class CurrencyCommandController {

	@Autowired
	private CurrencyService currencyService;
	
	@Autowired
	private CurrencyResourceAssembler assembler;
	

	@Autowired
	private CreatedResponseResourceAssembler responseAssembler;

	/**
	 * @param resource
	 * @param request
	 * @return
	 */
	@ApiOperation(value = "Create or Update Currency", response = CreatedResponseModel.class)
	@PostMapping(value = "createCurrency", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@PutMapping(value = "createCurrency", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<CreatedResponseModel> saveOrUpdate(@RequestBody CurrencyModel resource,
			HttpServletRequest request) {
		CurrencyVO CurrencyVO = assembler.fromModel(resource);
		CreateCurrencyEvent event = new CreateCurrencyEvent();
		event.setCurrencyVO(CurrencyVO);
		ResourceCreatedEvent resourceCreatedEvent = currencyService.saveOrUpdate(event);
		CreatedResponseModel createdResponseModel = responseAssembler.toModel(resourceCreatedEvent.getResponseVO());
		return new ResponseEntity<CreatedResponseModel>(createdResponseModel, HttpStatus.OK);
	}

	/**
	 * @param Currencyid
	 * @return
	 */
	@ApiOperation(value = "Delete Currency", response = CreatedResponseModel.class)
	@DeleteMapping(path = "/deleteCurrency/{currencyid}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<CreatedResponseModel> deleteById(@PathVariable("currencyid") Long currencyId) {
		ReadCurrencyEvent request = new ReadCurrencyEvent();
		request.setId(currencyId);
		ResourceDeletedEvent resourceDeletedEvent = currencyService.deleteById(request);
		CreatedResponseModel createdResponseModel = responseAssembler.toModel(resourceDeletedEvent.getResponseVO());
		return new ResponseEntity<CreatedResponseModel>(createdResponseModel, HttpStatus.OK);
	}

}
