package com.ideyalabs.cms.services.admin.app.command.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.cms.services.admin.service.TaxService;
import com.cms.services.admin.service.events.CreateTaxEvent;
import com.cms.services.admin.service.events.ReadTaxEvent;
import com.cms.services.admin.service.events.ResourceCreatedEvent;
import com.cms.services.admin.service.events.ResourceDeletedEvent;
import com.cms.services.admin.service.vo.TaxVO;
import com.ideyalabs.cms.services.admin.app.resource.TaxModel;
import com.ideyalabs.cms.services.admin.app.resource.CreatedResponseModel;
import com.ideyalabs.cms.services.admin.assembler.TaxResourceAssembler;
import com.ideyalabs.cms.services.admin.assembler.CreatedResponseResourceAssembler;

import io.swagger.annotations.ApiOperation;

/**
 * 
 * @author sanjeev P
 *
 */
@RestController("/v1/tax")
public class TaxCommandController {

	@Autowired
	private TaxService taxService;

	@Autowired
	private TaxResourceAssembler assembler;

	@Autowired
	private CreatedResponseResourceAssembler responseAssembler;

	/**
	 * @param resource
	 * @param request
	 * @return
	 */
	@ApiOperation(value = "Create or Update Tax", response = CreatedResponseModel.class)
	@PostMapping(value = "createTax", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@PutMapping(value = "createTax", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<CreatedResponseModel> saveOrUpdate(@RequestBody TaxModel resource,
			HttpServletRequest request) {
		TaxVO taxVO = assembler.fromModel(resource);
		CreateTaxEvent event = new CreateTaxEvent();
		event.setTaxVO(taxVO);
		ResourceCreatedEvent resourceCreatedEvent = taxService.saveOrUpdate(event);
		CreatedResponseModel createdResponseModel = responseAssembler.toModel(resourceCreatedEvent.getResponseVO());
		return new ResponseEntity<CreatedResponseModel>(createdResponseModel, HttpStatus.OK);
	}

	/**
	 * @param Taxid
	 * @return
	 */
	@ApiOperation(value = "Delete Tax", response = CreatedResponseModel.class)
	@DeleteMapping(value = "/deleteTax/{taxid}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<CreatedResponseModel> deleteById(@PathVariable("taxid") Long taxId) {
		ReadTaxEvent request = new ReadTaxEvent();
		request.setId(taxId);
		ResourceDeletedEvent resourceDeletedEvent = taxService.deleteById(request);
		CreatedResponseModel createdResponseModel = responseAssembler.toModel(resourceDeletedEvent.getResponseVO());
		return new ResponseEntity<CreatedResponseModel>(createdResponseModel, HttpStatus.OK);
	}
}
