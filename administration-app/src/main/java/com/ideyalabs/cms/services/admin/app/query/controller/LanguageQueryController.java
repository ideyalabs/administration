package com.ideyalabs.cms.services.admin.app.query.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.PagedModel;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.cms.services.admin.common.QueryParameterConstants;
import com.cms.services.admin.service.LanguageService;
import com.cms.services.admin.service.events.PageReadEvent;
import com.cms.services.admin.service.events.ReadLanguageEvent;
import com.cms.services.admin.service.vo.LanguageVO;
import com.ideyalabs.cms.services.admin.app.resource.LanguageModel;
import com.ideyalabs.cms.services.admin.assembler.LanguageResourceAssembler;

import io.swagger.annotations.ApiOperation;

/**
 * October 15, 2020
 * 
 * @author Sanjeev P
 *
 */
@RestController
@RequestMapping("/v1/language")
public class LanguageQueryController {

	@Autowired
	private LanguageService languageService;

	@Autowired
	private LanguageResourceAssembler assembler;

	/**
	 * @param sortDirection
	 * @param sortColumnName
	 * @param pageable
	 * @param pagedAssembler
	 * @param id
	 * @param firstName
	 * @param httpServletRequest
	 * @return
	 */
	@ApiOperation(value = "View list of Cities", response = LanguageModel.class, responseContainer = "List")
	@RequestMapping(value = "viewall", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<PagedModel<LanguageModel>> viewAll(
			@RequestParam(value = QueryParameterConstants.SORT_DIRECTION, required = false) String sortDirection,
			@RequestParam(value = QueryParameterConstants.SORT_COLUMN_NAME, required = false) String sortColumnName,
			@PageableDefault(value = Integer.MAX_VALUE) Pageable pageable,
			PagedResourcesAssembler<LanguageVO> pagedAssembler,
			@RequestParam(value = QueryParameterConstants.ID, required = false) Long id,
			HttpServletRequest httpServletRequest) {

		ReadLanguageEvent request = new ReadLanguageEvent();
		request.setId(id);
		request.setPageable(pageable);
		request.setSortDirection(sortDirection);
		request.setSortColumnName(sortColumnName);
		PageReadEvent<LanguageVO> event = languageService.viewAll(request);
		Page<LanguageVO> page = event.getPage();
		PagedModel<LanguageModel> pagedResources = pagedAssembler.toModel(page, assembler);
		return new ResponseEntity<>(pagedResources, HttpStatus.OK);
	}

	/**
	 * @param languageid
	 * @return
	 */
	@ApiOperation(value = "Get Language By Id", response = LanguageModel.class)
	@DeleteMapping(path = "/languagebyid/{languageid}")
	public ResponseEntity<LanguageModel> findById(@PathVariable("languageid") Long LanguageId) {
		ReadLanguageEvent request = new ReadLanguageEvent();
		request.setId(LanguageId);
		LanguageVO vo = languageService.findById(request);
		LanguageModel model = assembler.toModel(vo);
		return new ResponseEntity<>(model, HttpStatus.OK);

	}
}
