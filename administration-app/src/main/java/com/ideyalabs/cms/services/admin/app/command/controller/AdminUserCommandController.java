package com.ideyalabs.cms.services.admin.app.command.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.cms.services.admin.service.AdminUserService;
import com.cms.services.admin.service.events.CreateAdminUserEvent;
import com.cms.services.admin.service.events.ReadAdminUserEvent;
import com.cms.services.admin.service.events.ResourceCreatedEvent;
import com.cms.services.admin.service.events.ResourceDeletedEvent;
import com.cms.services.admin.service.vo.AdminUserVO;
import com.ideyalabs.cms.services.admin.app.resource.AdminUserModel;
import com.ideyalabs.cms.services.admin.app.resource.CreatedResponseModel;
import com.ideyalabs.cms.services.admin.assembler.AdminUserResourceAssembler;
import com.ideyalabs.cms.services.admin.assembler.CreatedResponseResourceAssembler;

import io.swagger.annotations.ApiOperation;

/**
 * 
 * @author sanjeev P
 *
 */
@RestController("/v1/adminuser")
public class AdminUserCommandController {

	@Autowired
	private AdminUserService adminUserService;

	@Autowired
	private AdminUserResourceAssembler assembler;

	@Autowired
	private CreatedResponseResourceAssembler responseAssembler;

	/**
	 * @param resource
	 * @param request
	 * @return
	 */
	@ApiOperation(value = "Create or Update AdminUser", response = CreatedResponseModel.class)
	@PostMapping(value = "createAdminUser", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@PutMapping(value = "createAdminUser", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<CreatedResponseModel> saveOrUpdate(@RequestBody AdminUserModel resource,
			HttpServletRequest request) {
		AdminUserVO adminUserVO = assembler.fromModel(resource);
		CreateAdminUserEvent event = new CreateAdminUserEvent();
		event.setAdminUserVO(adminUserVO);
		ResourceCreatedEvent resourceCreatedEvent = adminUserService.saveOrUpdate(event);
		CreatedResponseModel createdResponseModel = responseAssembler.toModel(resourceCreatedEvent.getResponseVO());
		return new ResponseEntity<CreatedResponseModel>(createdResponseModel, HttpStatus.OK);
	}

	/**
	 * @param AdminUserid
	 * @return
	 */
	@ApiOperation(value = "Delete AdminUser", response = CreatedResponseModel.class)
	@DeleteMapping(value = "/deleteAdminUser/{adminUserid}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<CreatedResponseModel> deleteById(@PathVariable("adminUserid") Long adminUserid) {
		ReadAdminUserEvent request = new ReadAdminUserEvent();
		request.setId(adminUserid);
		ResourceDeletedEvent resourceDeletedEvent = adminUserService.deleteById(request);
		CreatedResponseModel createdResponseModel = responseAssembler.toModel(resourceDeletedEvent.getResponseVO());
		return new ResponseEntity<CreatedResponseModel>(createdResponseModel, HttpStatus.OK);
	}
}
