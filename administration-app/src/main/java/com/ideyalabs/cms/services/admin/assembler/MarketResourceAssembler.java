package com.ideyalabs.cms.services.admin.assembler;

import java.util.ArrayList;
import java.util.List;

import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import org.springframework.stereotype.Component;

import com.cms.services.admin.service.vo.MarketVO;
import com.ideyalabs.cms.services.admin.app.command.controller.MarketCommandController;
import com.ideyalabs.cms.services.admin.app.resource.CountryModel;
import com.ideyalabs.cms.services.admin.app.resource.MarketModel;

/**
 * October 17, 2020
 * 
 * @author Sanjeev P
 *
 */

@Component
public class MarketResourceAssembler extends RepresentationModelAssemblerSupport<MarketVO, MarketModel> {

	/**
	 * @param controllerClass
	 * @param resourceType
	 */
	public MarketResourceAssembler() {
		super(MarketCommandController.class, MarketModel.class);
		// TODO Auto-generated constructor stub
	}

	/**
	 *
	 */
	@Override
	public MarketModel toModel(MarketVO vo) {

		return MarketModel.builder().id(vo.getId()).title(vo.getTitle()).currency(vo.getCurrency())
				.taxInPrice(vo.isTaxInPrice()).status(vo.isStatus()).createdOn(vo.getCreatedOn())
				.updatedOn(vo.getUpdatedOn()).build();
	}

	/**
	 * @param model
	 * @return
	 */
	public MarketVO fromModel(MarketModel model) {

 		return MarketVO.builder().id(model.getId()).title(model.getTitle()).currency(model.getCurrency())
				.taxInPrice(model.isTaxInPrice()).status(model.isStatus()).createdOn(model.getCreatedOn())
				.updatedOn(model.getUpdatedOn()).build();
	}

}
