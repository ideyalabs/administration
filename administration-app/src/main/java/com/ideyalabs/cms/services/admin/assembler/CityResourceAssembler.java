package com.ideyalabs.cms.services.admin.assembler;

import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import org.springframework.stereotype.Component;

import com.cms.services.admin.service.vo.CityVO;
import com.ideyalabs.cms.services.admin.app.command.controller.CityCommandController;
import com.ideyalabs.cms.services.admin.app.resource.CityModel;

/**
 * October 1, 2020
 * 
 * @author Sanjeev P
 *
 */

@Component
public class CityResourceAssembler extends RepresentationModelAssemblerSupport<CityVO, CityModel> {

	/**
	 * @param controllerClass
	 * @param resourceType
	 */
	public CityResourceAssembler() {
		super(CityCommandController.class, CityModel.class);
		// TODO Auto-generated constructor stub
	}

	/**
	 *
	 */
	@Override
	public CityModel toModel(CityVO vo) {

		return CityModel.builder().id(vo.getId()).name(vo.getName()).countryCode(vo.getCountryCode())
				.coordinates(vo.getCoordinates()).createdOn(vo.getCreatedOn()).updatedOn(vo.getUpdatedOn()).build();
	}

	/**
	 * @param model
	 * @return
	 */
	public CityVO fromModel(CityModel model) {

		return CityVO.builder().id(model.getId()).name(model.getName()).countryCode(model.getCountryCode())
				.coordinates(model.getCoordinates()).createdOn(model.getCreatedOn()).updatedOn(model.getUpdatedOn())
				.build();
	}

}
