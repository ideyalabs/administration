package com.ideyalabs.cms.services.admin.app.query.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.PagedModel;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.cms.services.admin.common.QueryParameterConstants;
import com.cms.services.admin.service.CurrencyService;
import com.cms.services.admin.service.events.PageReadEvent;
import com.cms.services.admin.service.events.ReadCurrencyEvent;
import com.cms.services.admin.service.vo.CurrencyVO;
import com.ideyalabs.cms.services.admin.app.resource.CountryModel;
import com.ideyalabs.cms.services.admin.app.resource.CurrencyModel;
import com.ideyalabs.cms.services.admin.assembler.CurrencyResourceAssembler;

import io.swagger.annotations.ApiOperation;

/**
 * October 15, 2020
 * 
 * @author Sanjeev P
 *
 */
@RestController
@RequestMapping("/v1/currency")
public class CurrencyQueryController {

	@Autowired
	private CurrencyService currencyService;

	@Autowired
	private CurrencyResourceAssembler assembler;

	/**
	 * @param sortDirection
	 * @param sortColumnName
	 * @param pageable
	 * @param pagedAssembler
	 * @param id
	 * @param firstName
	 * @param httpServletRequest
	 * @return
	 */
	@ApiOperation(value = "View list of Currencies", response = CountryModel.class, responseContainer = "List")
	@RequestMapping(value = "viewall", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<PagedModel<CurrencyModel>> viewAll(
			@RequestParam(value = QueryParameterConstants.SORT_DIRECTION, required = false) String sortDirection,
			@RequestParam(value = QueryParameterConstants.SORT_COLUMN_NAME, required = false) String sortColumnName,
			@PageableDefault(value = Integer.MAX_VALUE) Pageable pageable,
			PagedResourcesAssembler<CurrencyVO> pagedAssembler,
			@RequestParam(value = QueryParameterConstants.ID, required = false) Long id,
			@RequestParam(value = QueryParameterConstants.FIRST_NAME, required = false) String firstName,
			HttpServletRequest httpServletRequest) {

		ReadCurrencyEvent request = new ReadCurrencyEvent();
		request.setId(id);
		request.setPageable(pageable);
		request.setSortDirection(sortDirection);
		request.setSortColumnName(sortColumnName);
		PageReadEvent<CurrencyVO> event = currencyService.viewAll(request);
		Page<CurrencyVO> page = event.getPage();
		PagedModel<CurrencyModel> pagedResources = pagedAssembler.toModel(page, assembler);
		return new ResponseEntity<>(pagedResources, HttpStatus.OK);
	}

	/**
	 * @param currencyid
	 * @return
	 */
	@ApiOperation(value = "Get Currency By Id", response = CountryModel.class)
	@GetMapping(path = "/currencybyid/{currencyid}")
	public ResponseEntity<CurrencyModel> findById(@PathVariable("currencyId") Long currencyId) {
		ReadCurrencyEvent request = new ReadCurrencyEvent();
		request.setId(currencyId);
		CurrencyVO vo = currencyService.findById(request);
		CurrencyModel model = assembler.toModel(vo);
		return new ResponseEntity<>(model, HttpStatus.OK);

	}
}
