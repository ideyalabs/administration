package com.ideyalabs.cms.services.admin.assembler;

import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import org.springframework.stereotype.Component;

import com.cms.services.admin.service.vo.CurrencyVO;
import com.ideyalabs.cms.services.admin.app.command.controller.CurrencyCommandController;
import com.ideyalabs.cms.services.admin.app.resource.CurrencyModel;

/**
 * October 17, 2020
 * 
 * @author Sanjeev P
 *
 */

@Component
public class CurrencyResourceAssembler extends RepresentationModelAssemblerSupport<CurrencyVO, CurrencyModel> {

	/**
	 * @param controllerClass
	 * @param resourceType
	 */
	public CurrencyResourceAssembler() {
		super(CurrencyCommandController.class, CurrencyModel.class);
		// TODO Auto-generated constructor stub
	}

	/**
	 *
	 */
	@Override
	public CurrencyModel toModel(CurrencyVO vo) {

		return CurrencyModel.builder().id(vo.getId()).description(vo.getDescription()).currencyCode(vo.getCurrencyCode())
				.wholeSymbol(vo.getWholeSymbol()).wholePosition(vo.getWholePosition())
				.fractionSymbol(vo.getFractionSymbol()).places(vo.getPlaces()).status(vo.isStatus()).build();
	}

	/**
	 * @param model
	 * @return
	 */
	public CurrencyVO fromModel(CurrencyModel model) {

		return CurrencyVO.builder().id(model.getId()).description(model.getDescription()).currencyCode(model.getCurrencyCode())
				.wholeSymbol(model.getWholeSymbol()).wholePosition(model.getWholePosition())
				.fractionSymbol(model.getFractionSymbol()).places(model.getPlaces()).status(model.isStatus()).build();
	}

}
