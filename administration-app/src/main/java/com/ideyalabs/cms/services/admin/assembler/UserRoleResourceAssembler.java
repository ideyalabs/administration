package com.ideyalabs.cms.services.admin.assembler;

import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import org.springframework.stereotype.Component;

import com.cms.services.admin.service.vo.UserRoleVO;
import com.ideyalabs.cms.services.admin.app.command.controller.UserRoleCommandController;
import com.ideyalabs.cms.services.admin.app.resource.UserRoleModel;

/**
 * October 1, 2020
 * 
 * @author Sanjeev P
 *
 */

@Component
public class UserRoleResourceAssembler extends RepresentationModelAssemblerSupport<UserRoleVO, UserRoleModel> {

	/**
	 * @param controllerClass
	 * @param resourceType
	 */
	public UserRoleResourceAssembler() {
		super(UserRoleCommandController.class, UserRoleModel.class);
		// TODO Auto-generated constructor stub
	}

	/**
	 *
	 */
	@Override
	public UserRoleModel toModel(UserRoleVO vo) {

		return UserRoleModel.builder().id(vo.getId()).description(vo.getDescription())
				.allowAllPermissions(vo.isAllowAllPermissions()).build();
	}

	/**
	 * @param model
	 * @return
	 */
	public UserRoleVO fromModel(UserRoleModel model) {

		return UserRoleVO.builder().id(model.getId()).description(model.getDescription())
				.allowAllPermissions(model.isAllowAllPermissions()).build();
	}

}
