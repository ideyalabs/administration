package com.ideyalabs.cms.services.admin.assembler;

import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import org.springframework.stereotype.Component;

import com.cms.services.admin.service.vo.ResponseVO;
import com.ideyalabs.cms.services.admin.app.command.controller.CountryCommandController;
import com.ideyalabs.cms.services.admin.app.resource.CreatedResponseModel;

/**
 * October 15, 2020
 * 
 * @author Sanjeev P
 *
 */
@Component
public class CreatedResponseResourceAssembler
		extends RepresentationModelAssemblerSupport<ResponseVO, CreatedResponseModel> {
	
	/**
	 * 
	 */
	public CreatedResponseResourceAssembler() {
		super(CountryCommandController.class, CreatedResponseModel.class);
	}

	/**
	 *
	 */
	@Override
	public CreatedResponseModel toModel(ResponseVO responseVO) {
		return CreatedResponseModel.builder().code(responseVO.getCode()).message(responseVO.getMessage()).build();
	}

}
