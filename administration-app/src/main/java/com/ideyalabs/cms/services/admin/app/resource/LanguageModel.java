package com.ideyalabs.cms.services.admin.app.resource;

import java.util.Date;

import org.springframework.hateoas.RepresentationModel;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 * October 15, 2020
 * 
 * @author Sanjeev P
 *
 */
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
public class LanguageModel extends RepresentationModel<LanguageModel> {

	private Long id;
	private String name;
	private String shortCode;
	private boolean status;
	private Date createdOn;
	private Date updatedOn;

}
