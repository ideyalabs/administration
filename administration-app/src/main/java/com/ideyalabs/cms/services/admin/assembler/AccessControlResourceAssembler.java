package com.ideyalabs.cms.services.admin.assembler;

import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import org.springframework.stereotype.Component;

import com.cms.services.admin.service.vo.AccessControlVO;
import com.ideyalabs.cms.services.admin.app.command.controller.AccessControlCommandController;
import com.ideyalabs.cms.services.admin.app.resource.AccessControlModel;

/**
 * October 1, 2020
 * 
 * @author Sanjeev P
 *
 */

@Component
public class AccessControlResourceAssembler
		extends RepresentationModelAssemblerSupport<AccessControlVO, AccessControlModel> {

	/**
	 * @param controllerClass
	 * @param resourceType
	 */
	public AccessControlResourceAssembler() {
		super(AccessControlCommandController.class, AccessControlModel.class);
		// TODO Auto-generated constructor stub
	}

	/**
	 *
	 */
	@Override
	public AccessControlModel toModel(AccessControlVO vo) {

		return AccessControlModel.builder().id(vo.getId()).alias(vo.getAlias()).visibleInGroups(vo.isVisibleInGroups())
				.build();
	}

	/**
	 * @param model
	 * @return
	 */
	public AccessControlVO fromModel(AccessControlModel model) {

		return AccessControlVO.builder().id(model.getId()).alias(model.getAlias())
				.visibleInGroups(model.isVisibleInGroups()).build();
	}

}
