package com.ideyalabs.cms.services.admin.app.command.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.cms.services.admin.service.CityService;
import com.cms.services.admin.service.events.CreateCityEvent;
import com.cms.services.admin.service.events.ReadCityEvent;
import com.cms.services.admin.service.events.ResourceCreatedEvent;
import com.cms.services.admin.service.events.ResourceDeletedEvent;
import com.cms.services.admin.service.vo.CityVO;
import com.ideyalabs.cms.services.admin.app.resource.CityModel;
import com.ideyalabs.cms.services.admin.app.resource.CreatedResponseModel;
import com.ideyalabs.cms.services.admin.assembler.CityResourceAssembler;
import com.ideyalabs.cms.services.admin.assembler.CreatedResponseResourceAssembler;

import io.swagger.annotations.ApiOperation;

/**
 * 
 * @author sanjeev P
 *
 */
@RestController("/v1/city")
public class CityCommandController {

	@Autowired
	private CityService cityService;

	@Autowired
	private CityResourceAssembler assembler;

	@Autowired
	private CreatedResponseResourceAssembler responseAssembler;

	/**
	 * @param resource
	 * @param request
	 * @return
	 */
	@ApiOperation(value = "Create or Update City", response = CreatedResponseModel.class)
	@PostMapping(value = "createCity", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@PutMapping(value = "createCity", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<CreatedResponseModel> saveOrUpdate(@RequestBody CityModel resource,
			HttpServletRequest request) {
		CityVO cityVO = assembler.fromModel(resource);
		CreateCityEvent event = new CreateCityEvent();
		event.setCityVO(cityVO);
		ResourceCreatedEvent resourceCreatedEvent = cityService.saveOrUpdate(event);
		CreatedResponseModel createdResponseModel = responseAssembler.toModel(resourceCreatedEvent.getResponseVO());
		return new ResponseEntity<CreatedResponseModel>(createdResponseModel, HttpStatus.OK);
	}

	/**
	 * @param cityid
	 * @return
	 */
	@ApiOperation(value = "Delete City", response = CreatedResponseModel.class)
	@DeleteMapping(value = "/deleteCity/{cityid}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<CreatedResponseModel> deleteById(@PathVariable("cityid") Long cityid) {
		ReadCityEvent request = new ReadCityEvent();
		request.setId(cityid);
		ResourceDeletedEvent resourceDeletedEvent = cityService.deleteById(request);
		CreatedResponseModel createdResponseModel = responseAssembler.toModel(resourceDeletedEvent.getResponseVO());
		return new ResponseEntity<CreatedResponseModel>(createdResponseModel, HttpStatus.OK);
	}
}
