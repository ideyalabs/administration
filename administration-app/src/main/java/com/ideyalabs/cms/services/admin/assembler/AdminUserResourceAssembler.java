package com.ideyalabs.cms.services.admin.assembler;

import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import org.springframework.stereotype.Component;

import com.cms.services.admin.service.vo.AdminUserVO;
import com.ideyalabs.cms.services.admin.app.command.controller.AdminUserCommandController;
import com.ideyalabs.cms.services.admin.app.resource.AdminUserModel;

/**
 * October 15, 2020
 * 
 * @author Sanjeev P
 *
 */

@Component
public class AdminUserResourceAssembler extends RepresentationModelAssemblerSupport<AdminUserVO, AdminUserModel> {

	/**
	 * @param controllerClass
	 * @param resourceType
	 */
	public AdminUserResourceAssembler() {
		super(AdminUserCommandController.class, AdminUserModel.class);
		// TODO Auto-generated constructor stub
	}

	/**
	 *
	 */
	@Override
	public AdminUserModel toModel(AdminUserVO vo) {

		return AdminUserModel.builder().id(vo.getId()).name(vo.getName()).emailId(vo.getEmailId())
				.loginID(vo.getLoginID()).role(vo.getRole()).lastAccessed(vo.getLastAccessed()).status(vo.isStatus())
				.password(vo.getPassword()).confirmPassword(vo.getConfirmPassword()).build();
	}

	/**
	 * @param model
	 * @return
	 */
	public AdminUserVO fromModel(AdminUserModel model) {

		return AdminUserVO.builder().id(model.getId()).name(model.getName()).emailId(model.getEmailId())
				.loginID(model.getLoginID()).role(model.getRole()).lastAccessed(model.getLastAccessed())
				.status(model.isStatus()).build();
	}

}
