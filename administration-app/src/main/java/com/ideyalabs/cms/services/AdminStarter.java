package com.ideyalabs.cms.services;

/*
 * Spring boot application generate using ideyalabs archetype
 * 
 */

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan( {"com.ideyalabs","com.cms"} )
public class AdminStarter{

	public static void main(String[] args) {
		SpringApplication.run(AdminStarter.class, args);
    }

}