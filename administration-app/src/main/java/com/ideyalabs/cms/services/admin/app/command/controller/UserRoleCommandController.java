package com.ideyalabs.cms.services.admin.app.command.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.cms.services.admin.service.UserRoleService;
import com.cms.services.admin.service.events.CreateUserRoleEvent;
import com.cms.services.admin.service.events.ReadUserRoleEvent;
import com.cms.services.admin.service.events.ResourceCreatedEvent;
import com.cms.services.admin.service.events.ResourceDeletedEvent;
import com.cms.services.admin.service.vo.UserRoleVO;
import com.ideyalabs.cms.services.admin.app.resource.UserRoleModel;
import com.ideyalabs.cms.services.admin.app.resource.CreatedResponseModel;
import com.ideyalabs.cms.services.admin.assembler.UserRoleResourceAssembler;
import com.ideyalabs.cms.services.admin.assembler.CreatedResponseResourceAssembler;

import io.swagger.annotations.ApiOperation;

/**
 * 
 * @author sanjeev P
 *
 */
@RestController("/v1/userRole")
public class UserRoleCommandController {

	@Autowired
	private UserRoleService userRoleService;

	@Autowired
	private UserRoleResourceAssembler assembler;

	@Autowired
	private CreatedResponseResourceAssembler responseAssembler;

	/**
	 * @param resource
	 * @param request
	 * @return
	 */
	@ApiOperation(value = "Create or Update UserRole", response = CreatedResponseModel.class)
	@PostMapping(value = "createUserRole", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@PutMapping(value = "createUserRole", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<CreatedResponseModel> saveOrUpdate(@RequestBody UserRoleModel resource,
			HttpServletRequest request) {
		UserRoleVO userRoleVO = assembler.fromModel(resource);
		CreateUserRoleEvent event = new CreateUserRoleEvent();
		event.setUserRoleVO(userRoleVO);
		ResourceCreatedEvent resourceCreatedEvent = userRoleService.saveOrUpdate(event);
		CreatedResponseModel createdResponseModel = responseAssembler.toModel(resourceCreatedEvent.getResponseVO());
		return new ResponseEntity<CreatedResponseModel>(createdResponseModel, HttpStatus.OK);
	}

	/**
	 * @param UserRoleid
	 * @return
	 */
	@ApiOperation(value = "Delete UserRole", response = CreatedResponseModel.class)
	@DeleteMapping(value = "/deleteUserRole/{userroleid}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<CreatedResponseModel> deleteById(@PathVariable("userroleid") Long userRoleId) {
		ReadUserRoleEvent request = new ReadUserRoleEvent();
		request.setId(userRoleId);
		ResourceDeletedEvent resourceDeletedEvent = userRoleService.deleteById(request);
		CreatedResponseModel createdResponseModel = responseAssembler.toModel(resourceDeletedEvent.getResponseVO());
		return new ResponseEntity<CreatedResponseModel>(createdResponseModel, HttpStatus.OK);
	}
}
