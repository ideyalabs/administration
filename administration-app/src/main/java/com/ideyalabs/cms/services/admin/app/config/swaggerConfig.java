package com.ideyalabs.cms.services.admin.app.config;

import static springfox.documentation.builders.PathSelectors.regex;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.hateoas.client.LinkDiscoverer;
import org.springframework.hateoas.client.LinkDiscoverers;
import org.springframework.hateoas.mediatype.collectionjson.CollectionJsonLinkDiscoverer;
import org.springframework.plugin.core.SimplePluginRegistry;
import org.springframework.web.bind.annotation.RestController;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * October 15, 2020
 * 
 * @author Sanjeev P
 *
 */
@Configuration
@EnableSwagger2
public class swaggerConfig {

	/**
	 * @return
	 */
	@Bean
	public Docket demoApi() {
		return new Docket(DocumentationType.SWAGGER_2).groupName("demo").select()
				.apis(RequestHandlerSelectors.withClassAnnotation(RestController.class)).paths(regex("/v1/demo/"))
				.build().apiInfo(metadata());
	}

	/**
	 * @return
	 */
	@Bean
	public Docket otherApi() {
		return new Docket(DocumentationType.SWAGGER_2).groupName("other").select()
				.apis(RequestHandlerSelectors.withClassAnnotation(RestController.class)).paths(PathSelectors.any())
				.build().apiInfo(metadata());
	}

	/**
	 * @return
	 */
	private ApiInfo metadata() {
		return new ApiInfoBuilder().title("CMS Application").description("API reference guide for developers")
				.termsOfServiceUrl("https://www.cms.com/").version("1.0").build();
	}

	/**
	 * @return
	 */
	@Bean
	public LinkDiscoverers discoverers() {
		List<LinkDiscoverer> plugins = new ArrayList<>();
		plugins.add(new CollectionJsonLinkDiscoverer());
		return new LinkDiscoverers(SimplePluginRegistry.create(plugins));

	}
}
