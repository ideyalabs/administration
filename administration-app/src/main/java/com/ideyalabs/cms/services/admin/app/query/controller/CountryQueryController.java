package com.ideyalabs.cms.services.admin.app.query.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.PagedModel;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.cms.services.admin.common.QueryParameterConstants;
import com.cms.services.admin.service.CountryService;
import com.cms.services.admin.service.events.PageReadEvent;
import com.cms.services.admin.service.events.ReadCountryEvent;
import com.cms.services.admin.service.vo.CountryVO;
import com.ideyalabs.cms.services.admin.app.resource.CountryModel;
import com.ideyalabs.cms.services.admin.assembler.CountryResourceAssembler;

import io.swagger.annotations.ApiOperation;

/**
 * October 15, 2020
 * 
 * @author Sanjeev P
 *
 */
@RestController
@RequestMapping("/v1/country")
public class CountryQueryController {

	@Autowired
	private CountryService countryService;

	@Autowired
	private CountryResourceAssembler assembler;

	/**
	 * @param sortDirection
	 * @param sortColumnName
	 * @param pageable
	 * @param pagedAssembler
	 * @param id
	 * @param firstName
	 * @param httpServletRequest
	 * @return
	 */
	@ApiOperation(value = "View list of Countries", response = CountryModel.class, responseContainer = "List")
	@RequestMapping(value = "viewall", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<PagedModel<CountryModel>> viewAll(
			@RequestParam(value = QueryParameterConstants.SORT_DIRECTION, required = false) String sortDirection,
			@RequestParam(value = QueryParameterConstants.SORT_COLUMN_NAME, required = false) String sortColumnName,
			@PageableDefault(value = Integer.MAX_VALUE) Pageable pageable,
			PagedResourcesAssembler<CountryVO> pagedAssembler,
			@RequestParam(value = QueryParameterConstants.ID, required = false) Long id,
			@RequestParam(value = QueryParameterConstants.FIRST_NAME, required = false) String firstName,
			HttpServletRequest httpServletRequest) {

		ReadCountryEvent request = new ReadCountryEvent();
		request.setId(id);
		request.setPageable(pageable);
		request.setSortDirection(sortDirection);
		request.setSortColumnName(sortColumnName);
		PageReadEvent<CountryVO> event = countryService.viewAll(request);
		Page<CountryVO> page = event.getPage();
		PagedModel<CountryModel> pagedResources = pagedAssembler.toModel(page, assembler);
		return new ResponseEntity<>(pagedResources, HttpStatus.OK);
	}

	/**
	 * @param countryid
	 * @return
	 */
	@ApiOperation(value = "Get Country By Id", response = CountryModel.class)
	@GetMapping(path = "/countrybyid/{countryid}")
	public ResponseEntity<CountryModel> findById(@PathVariable("countryid") Long countryId) {
		ReadCountryEvent request = new ReadCountryEvent();
		request.setId(countryId);
		CountryVO vo = countryService.findById(request);
		CountryModel model = assembler.toModel(vo);
		return new ResponseEntity<>(model, HttpStatus.OK);

	}
}
