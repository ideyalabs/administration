package com.ideyalabs.cms.services.admin.assembler;

import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import org.springframework.stereotype.Component;

import com.cms.services.admin.service.vo.TaxVO;
import com.ideyalabs.cms.services.admin.app.command.controller.TaxCommandController;
import com.ideyalabs.cms.services.admin.app.resource.TaxModel;

/**
 * October 1, 2020
 * 
 * @author Sanjeev P
 *
 */

@Component
public class TaxResourceAssembler extends RepresentationModelAssemblerSupport<TaxVO, TaxModel> {

	/**
	 * @param controllerClass
	 * @param resourceType
	 */
	public TaxResourceAssembler() {
		super(TaxCommandController.class, TaxModel.class);
		// TODO Auto-generated constructor stub
	}

	/**
	 *
	 */
	@Override
	public TaxModel toModel(TaxVO vo) {

		return TaxModel.builder().id(vo.getId()).country(vo.getCountry()).corporateTax(vo.getCorporateTax())
				.privateTax(vo.getPrivateTax()).build();
	}

	/**
	 * @param model
	 * @return
	 */
	public TaxVO fromModel(TaxModel model) {

		return TaxVO.builder().id(model.getId()).country(model.getCountry()).corporateTax(model.getCorporateTax())
				.privateTax(model.getPrivateTax()).build();
	}

}
