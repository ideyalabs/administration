package com.ideyalabs.cms.services.admin.assembler;

import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import org.springframework.stereotype.Component;

import com.cms.services.admin.service.vo.LanguageVO;
import com.ideyalabs.cms.services.admin.app.command.controller.LanguageCommandController;
import com.ideyalabs.cms.services.admin.app.resource.LanguageModel;

/**
 * October 1, 2020
 * 
 * @author Sanjeev P
 *
 */

@Component
public class LanguageResourceAssembler extends RepresentationModelAssemblerSupport<LanguageVO, LanguageModel> {

	/**
	 * @param controllerClass
	 * @param resourceType
	 */
	public LanguageResourceAssembler() {
		super(LanguageCommandController.class, LanguageModel.class);
		// TODO Auto-generated constructor stub
	}

	/**
	 *
	 */
	@Override
	public LanguageModel toModel(LanguageVO vo) {

		return LanguageModel.builder().id(vo.getId()).name(vo.getName()).shortCode(vo.getShortCode())
				.status(vo.isStatus()).createdOn(vo.getCreatedOn()).updatedOn(vo.getUpdatedOn()).build();
	}

	/**
	 * @param model
	 * @return
	 */
	public LanguageVO fromModel(LanguageModel model) {

		return LanguageVO.builder().id(model.getId()).name(model.getName()).shortCode(model.getShortCode())
				.status(model.isStatus()).createdOn(model.getCreatedOn()).updatedOn(model.getUpdatedOn()).build();
	}

}
