package com.ideyalabs.cms.services.admin.app.query.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.PagedModel;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.cms.services.admin.common.QueryParameterConstants;
import com.cms.services.admin.service.AccessControlService;
import com.cms.services.admin.service.events.PageReadEvent;
import com.cms.services.admin.service.events.ReadAccessControlEvent;
import com.cms.services.admin.service.vo.AccessControlVO;
import com.ideyalabs.cms.services.admin.app.resource.AccessControlModel;
import com.ideyalabs.cms.services.admin.assembler.AccessControlResourceAssembler;

import io.swagger.annotations.ApiOperation;

/**
 * October 15, 2020
 * 
 * @author Sanjeev P
 *
 */
@RestController
@RequestMapping("/v1/accessControl")
public class AccessControlQueryController {

	@Autowired
	private AccessControlService accessControlService;

	@Autowired
	private AccessControlResourceAssembler assembler;

	/**
	 * @param sortDirection
	 * @param sortColumnName
	 * @param pageable
	 * @param pagedAssembler
	 * @param id
	 * @param firstName
	 * @param httpServletRequest
	 * @return
	 */
	@ApiOperation(value = "View list of AccessControls", response = AccessControlModel.class, responseContainer = "List")
	@RequestMapping(value = "viewall", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<PagedModel<AccessControlModel>> viewAll(
			@RequestParam(value = QueryParameterConstants.SORT_DIRECTION, required = false) String sortDirection,
			@RequestParam(value = QueryParameterConstants.SORT_COLUMN_NAME, required = false) String sortColumnName,
			@PageableDefault(value = Integer.MAX_VALUE) Pageable pageable,
			PagedResourcesAssembler<AccessControlVO> pagedAssembler,
			@RequestParam(value = QueryParameterConstants.ID, required = false) Long id,
			HttpServletRequest httpServletRequest) {

		ReadAccessControlEvent request = new ReadAccessControlEvent();
		request.setId(id);
		request.setPageable(pageable);
		request.setSortDirection(sortDirection);
		request.setSortColumnName(sortColumnName);
		PageReadEvent<AccessControlVO> event = accessControlService.viewAll(request);
		Page<AccessControlVO> page = event.getPage();
		PagedModel<AccessControlModel> pagedResources = pagedAssembler.toModel(page, assembler);
		return new ResponseEntity<>(pagedResources, HttpStatus.OK);
	}

	/**
	 * @param accessControlid
	 * @return
	 */
	@ApiOperation(value = "Get AccessControl By Id", response = AccessControlModel.class)
	@GetMapping(path = "/{accesscontrolid}")
	public ResponseEntity<AccessControlModel> findById(@PathVariable("accesscontrolid") Long accessControlId) {
		ReadAccessControlEvent request = new ReadAccessControlEvent();
		request.setId(accessControlId);
		AccessControlVO vo = accessControlService.findById(request);
		AccessControlModel model = assembler.toModel(vo);
		return new ResponseEntity<>(model, HttpStatus.OK);

	}
}
