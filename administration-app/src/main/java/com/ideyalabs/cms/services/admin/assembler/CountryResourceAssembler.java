package com.ideyalabs.cms.services.admin.assembler;

import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import org.springframework.stereotype.Component;

import com.cms.services.admin.service.vo.CountryVO;
import com.ideyalabs.cms.services.admin.app.command.controller.CountryCommandController;
import com.ideyalabs.cms.services.admin.app.resource.CountryModel;

/**
 * October 1, 2020
 * 
 * @author Sanjeev P
 *
 */

@Component
public class CountryResourceAssembler extends RepresentationModelAssemblerSupport<CountryVO, CountryModel> {

	/**
	 * @param controllerClass
	 * @param resourceType
	 */
	public CountryResourceAssembler() {
		super(CountryCommandController.class, CountryModel.class);
		// TODO Auto-generated constructor stub
	}

	/**
	 *
	 */
	@Override
	public CountryModel toModel(CountryVO vo) {

		return CountryModel.builder().id(vo.getId()).name(vo.getName()).countryCode(vo.getCountryCode())
				.codeNumber(vo.getCodeNumber()).coordinates(vo.getCoordinates()).createdOn(vo.getCreatedOn())
				.updatedOn(vo.getUpdatedOn()).build();
	}

	/**
	 * @param model
	 * @return
	 */
	public CountryVO fromModel(CountryModel model) {

		return CountryVO.builder().id(model.getId()).name(model.getName()).countryCode(model.getCountryCode())
				.codeNumber(model.getCodeNumber()).coordinates(model.getCoordinates()).status(model.isStatus())
				.createdOn(model.getCreatedOn()).updatedOn(model.getUpdatedOn()).build();
	}

}
