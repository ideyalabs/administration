package com.ideyalabs.cms.services.admin.app.resource;

import java.util.Date;

import org.springframework.hateoas.RepresentationModel;

import com.ideyalabs.cms.services.datamodel.common.RoleEntity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 * October 15, 2020
 * 
 * @author Sanjeev P
 *
 */
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
public class AdminUserModel extends RepresentationModel<AdminUserModel> {

	private Long id;
	private String name;
	private String emailId;	
	private String loginID;
	private RoleEntity role;
	private String password;
	private String confirmPassword;
	private Date lastAccessed;
	private boolean status;

}
