package com.ideyalabs.cms.services.admin.app.command.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.cms.services.admin.service.CountryService;
import com.cms.services.admin.service.events.CreateCountryEvent;
import com.cms.services.admin.service.events.ReadCountryEvent;
import com.cms.services.admin.service.events.ResourceCreatedEvent;
import com.cms.services.admin.service.events.ResourceDeletedEvent;
import com.cms.services.admin.service.vo.CountryVO;
import com.ideyalabs.cms.services.admin.app.resource.CountryModel;
import com.ideyalabs.cms.services.admin.app.resource.CreatedResponseModel;
import com.ideyalabs.cms.services.admin.assembler.CountryResourceAssembler;
import com.ideyalabs.cms.services.admin.assembler.CreatedResponseResourceAssembler;

import io.swagger.annotations.ApiOperation;
/**
 * 
 * @author sanjeev P
 *
 */
@RestController("/v1/country")
public class CountryCommandController {

	@Autowired
	private CountryService countryService;
	
	@Autowired
	private CountryResourceAssembler assembler;
	

	@Autowired
	private CreatedResponseResourceAssembler responseAssembler;

	/**
	 * @param resource
	 * @param request
	 * @return
	 */
	@ApiOperation(value = "Create or Update Country", response = CreatedResponseModel.class)
	@PostMapping(value = "createCountry", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@PutMapping(value = "createCountry", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<CreatedResponseModel> saveOrUpdate(@RequestBody CountryModel resource,
			HttpServletRequest request) {
		CountryVO countryVO = assembler.fromModel(resource);
		CreateCountryEvent event = new CreateCountryEvent();
		event.setCountryVO(countryVO);
		ResourceCreatedEvent resourceCreatedEvent = countryService.saveOrUpdate(event);
		CreatedResponseModel createdResponseModel = responseAssembler.toModel(resourceCreatedEvent.getResponseVO());
		return new ResponseEntity<CreatedResponseModel>(createdResponseModel, HttpStatus.OK);
	}

	/**
	 * @param countryid
	 * @return
	 */
	@ApiOperation(value = "Delete Country", response = CreatedResponseModel.class)
	@DeleteMapping(path = "/deleteCountry/{countryid}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<CreatedResponseModel> deleteById(@PathVariable("countryid") Long countryId) {
		ReadCountryEvent request = new ReadCountryEvent();
		request.setId(countryId);
		ResourceDeletedEvent resourceDeletedEvent = countryService.deleteById(request);
		CreatedResponseModel createdResponseModel = responseAssembler.toModel(resourceDeletedEvent.getResponseVO());
		return new ResponseEntity<CreatedResponseModel>(createdResponseModel, HttpStatus.OK);
	}

}
