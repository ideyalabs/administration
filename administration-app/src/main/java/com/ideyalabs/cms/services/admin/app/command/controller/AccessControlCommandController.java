package com.ideyalabs.cms.services.admin.app.command.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.cms.services.admin.service.AccessControlService;
import com.cms.services.admin.service.events.CreateAccessControlEvent;
import com.cms.services.admin.service.events.ReadAccessControlEvent;
import com.cms.services.admin.service.events.ResourceCreatedEvent;
import com.cms.services.admin.service.events.ResourceDeletedEvent;
import com.cms.services.admin.service.vo.AccessControlVO;
import com.ideyalabs.cms.services.admin.app.resource.AccessControlModel;
import com.ideyalabs.cms.services.admin.app.resource.CreatedResponseModel;
import com.ideyalabs.cms.services.admin.assembler.AccessControlResourceAssembler;
import com.ideyalabs.cms.services.admin.assembler.CreatedResponseResourceAssembler;

import io.swagger.annotations.ApiOperation;

/**
 * 
 * @author sanjeev P
 *
 */
@RestController("/v1/accessControl")
public class AccessControlCommandController {

	@Autowired
	private AccessControlService accessControlService;

	@Autowired
	private AccessControlResourceAssembler assembler;

	@Autowired
	private CreatedResponseResourceAssembler responseAssembler;

	/**
	 * @param resource
	 * @param request
	 * @return
	 */
	@ApiOperation(value = "Create or Update AccessControl", response = CreatedResponseModel.class)
	@PostMapping(value = "createAccessControl", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@PutMapping(value = "createAccessControl", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<CreatedResponseModel> saveOrUpdate(@RequestBody AccessControlModel resource,
			HttpServletRequest request) {
		AccessControlVO AccessControlVO = assembler.fromModel(resource);
		CreateAccessControlEvent event = new CreateAccessControlEvent();
		event.setAccessControlVO(AccessControlVO);
		ResourceCreatedEvent resourceCreatedEvent = accessControlService.saveOrUpdate(event);
		CreatedResponseModel createdResponseModel = responseAssembler.toModel(resourceCreatedEvent.getResponseVO());
		return new ResponseEntity<CreatedResponseModel>(createdResponseModel, HttpStatus.OK);
	}

	/**
	 * @param AccessControlid
	 * @return
	 */
	@ApiOperation(value = "Delete AccessControl", response = CreatedResponseModel.class)
	@DeleteMapping(value = "/deleteAccessControl/{accesscontrolid}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<CreatedResponseModel> deleteById(@PathVariable("accesscontrolid") Long accessControlId) {
		ReadAccessControlEvent request = new ReadAccessControlEvent();
		request.setId(accessControlId);
		ResourceDeletedEvent resourceDeletedEvent = accessControlService.deleteById(request);
		CreatedResponseModel createdResponseModel = responseAssembler.toModel(resourceDeletedEvent.getResponseVO());
		return new ResponseEntity<CreatedResponseModel>(createdResponseModel, HttpStatus.OK);
	}
}
