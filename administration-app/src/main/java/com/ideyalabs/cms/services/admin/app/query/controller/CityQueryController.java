package com.ideyalabs.cms.services.admin.app.query.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.PagedModel;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.cms.services.admin.common.QueryParameterConstants;
import com.cms.services.admin.service.CityService;
import com.cms.services.admin.service.events.PageReadEvent;
import com.cms.services.admin.service.events.ReadCityEvent;
import com.cms.services.admin.service.vo.CityVO;
import com.ideyalabs.cms.services.admin.app.resource.CityModel;
import com.ideyalabs.cms.services.admin.assembler.CityResourceAssembler;

import io.swagger.annotations.ApiOperation;

/**
 * October 15, 2020
 * 
 * @author Sanjeev P
 *
 */
@RestController
@RequestMapping("/v1/city")
public class CityQueryController {

	@Autowired
	private CityService cityService;

	@Autowired
	private CityResourceAssembler assembler;

	/**
	 * @param sortDirection
	 * @param sortColumnName
	 * @param pageable
	 * @param pagedAssembler
	 * @param id
	 * @param firstName
	 * @param httpServletRequest
	 * @return
	 */
	@ApiOperation(value = "View list of Cities", response = CityModel.class, responseContainer = "List")
	@RequestMapping(value = "viewall", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<PagedModel<CityModel>> viewAll(
			@RequestParam(value = QueryParameterConstants.SORT_DIRECTION, required = false) String sortDirection,
			@RequestParam(value = QueryParameterConstants.SORT_COLUMN_NAME, required = false) String sortColumnName,
			@PageableDefault(value = Integer.MAX_VALUE) Pageable pageable,
			PagedResourcesAssembler<CityVO> pagedAssembler,
			@RequestParam(value = QueryParameterConstants.ID, required = false) Long id,
			HttpServletRequest httpServletRequest) {

		ReadCityEvent request = new ReadCityEvent();
		request.setId(id);
		request.setPageable(pageable);
		request.setSortDirection(sortDirection);
		request.setSortColumnName(sortColumnName);
		PageReadEvent<CityVO> event = cityService.viewAll(request);
		Page<CityVO> page = event.getPage();
		PagedModel<CityModel> pagedResources = pagedAssembler.toModel(page, assembler);
		return new ResponseEntity<>(pagedResources, HttpStatus.OK);
	}

	/**
	 * @param cityid
	 * @return
	 */
	@ApiOperation(value = "Get City By Id", response = CityModel.class)
	@GetMapping(path = "/citybyid/{cityid}")
	public ResponseEntity<CityModel> findById(@PathVariable("cityid") Long cityId) {
		ReadCityEvent request = new ReadCityEvent();
		request.setId(cityId);
		CityVO vo = cityService.findById(request);
		CityModel model = assembler.toModel(vo);
		return new ResponseEntity<>(model, HttpStatus.OK);

	}
}
