package com.ideyalabs.cms.services.admin.app.command.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.cms.services.admin.service.MarketService;
import com.cms.services.admin.service.events.CreateMarketEvent;
import com.cms.services.admin.service.events.ReadMarketEvent;
import com.cms.services.admin.service.events.ResourceCreatedEvent;
import com.cms.services.admin.service.events.ResourceDeletedEvent;
import com.cms.services.admin.service.vo.MarketVO;
import com.ideyalabs.cms.services.admin.app.resource.MarketModel;
import com.ideyalabs.cms.services.admin.app.resource.CreatedResponseModel;
import com.ideyalabs.cms.services.admin.assembler.MarketResourceAssembler;
import com.ideyalabs.cms.services.admin.assembler.CreatedResponseResourceAssembler;

import io.swagger.annotations.ApiOperation;
/**
 * 
 * @author sanjeev P
 *
 */
@RestController("/v1/market")
public class MarketCommandController {

	@Autowired
	private MarketService marketService;
	
	@Autowired
	private MarketResourceAssembler assembler;
	

	@Autowired
	private CreatedResponseResourceAssembler responseAssembler;

	/**
	 * @param resource
	 * @param request
	 * @return
	 */
	@ApiOperation(value = "Create or Update Market", response = CreatedResponseModel.class)
	@PostMapping(value = "createMarket", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@PutMapping(value = "createMarket", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<CreatedResponseModel> saveOrUpdate(@RequestBody MarketModel resource,
			HttpServletRequest request) {
		MarketVO MarketVO = assembler.fromModel(resource);
		CreateMarketEvent event = new CreateMarketEvent();
		event.setMarketVO(MarketVO);
		ResourceCreatedEvent resourceCreatedEvent = marketService.saveOrUpdate(event);
		CreatedResponseModel createdResponseModel = responseAssembler.toModel(resourceCreatedEvent.getResponseVO());
		return new ResponseEntity<CreatedResponseModel>(createdResponseModel, HttpStatus.OK);
	}

	/**
	 * @param Marketid
	 * @return
	 */
	@ApiOperation(value = "Delete Market", response = CreatedResponseModel.class)
	@DeleteMapping(path = "/deleteMarket/{marketid}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<CreatedResponseModel> deleteById(@PathVariable("marketid") Long marketId) {
		ReadMarketEvent request = new ReadMarketEvent();
		request.setId(marketId);
		ResourceDeletedEvent resourceDeletedEvent = marketService.deleteById(request);
		CreatedResponseModel createdResponseModel = responseAssembler.toModel(resourceDeletedEvent.getResponseVO());
		return new ResponseEntity<CreatedResponseModel>(createdResponseModel, HttpStatus.OK);
	}

}
